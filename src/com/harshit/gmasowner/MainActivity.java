package com.harshit.gmasowner;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.harshit.gmasowner.database.DatabaseHandler;

public class MainActivity extends FragmentActivity {

	Context currentContext;
	EditText etUserName,etPassword;
	Button btnLogin,onlinestore;
	
	public static SharedPreferences sp;
	public static String user_pref="UserDetails";
	public static String username="";
	public static String uri_login="http://www.getmeashop.net/mobile/login/", string_store;
	public static Cookie getCSRFCookie;
	private boolean status = false;
	public Cookie contextCookie;
	private String string_name;
	
	public static PersistentCookieStore cookieStore;
	public static boolean hasSession=false;
	
	boolean loginStatus=false;
	private ProgressDialog dialog;
	
	public static SharedPreferences dbPrefs;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		currentContext=this;
		setContentView(R.layout.activity_main);
		
		sp=this.getSharedPreferences(user_pref, Context.MODE_PRIVATE);
		cookieStore=new PersistentCookieStore(this.getApplicationContext());
		List<Cookie> savedCookies=cookieStore.getCookies();
		
		dbPrefs = this.getSharedPreferences(
			      "dbInfo", Context.MODE_PRIVATE);
		if(dbPrefs.contains("productCounter"))
			DatabaseHandler.productCounter=dbPrefs.getInt("productCounter", 0);
		if(dbPrefs.contains("categoryCounter"))
			DatabaseHandler.categoryCounter=dbPrefs.getInt("categoryCounter", 0);
		
		dialog = new ProgressDialog(this);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage("Please wait...");
        dialog.setCancelable(false);
		
		etUserName=(EditText)findViewById(R.id.etUserName);
		etPassword=(EditText)findViewById(R.id.etPassword);
		btnLogin=(Button)findViewById(R.id.buttonLogin);
		onlinestore = (Button)findViewById(R.id.onlinestore);
		onlinestore.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(currentContext,AddInfo.class);
				startActivity(i);
				
			}
		});
		
		btnLogin.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				username=etUserName.getText().toString();
				if(username.equals("")){
					Toast.makeText(currentContext,"Enter valid username", Toast.LENGTH_SHORT).show();
				}
				else{
					SharedPreferences sp=getSharedPreferences("Users",Context.MODE_PRIVATE);
					Editor editor=sp.edit();
					editor.putString("userName", username);
					editor.putString("password",etPassword.getText().toString());
					editor.commit();
		//			new GetNewCSRF().execute(OnlineStoreData.uri_login);
					new GetNewCSRF().execute(uri_login);
					//openWebView();
					
				}
			}
		});
		
		for(Cookie c:savedCookies){
			if(c.getName().equals("sessionid")){
				hasSession=true;
				Log.d("hassession", String.valueOf(hasSession));
			}
		}
		if(hasSession){
			//etUserName.setVisibility(View.INVISIBLE);
			//etPassword.setVisibility(View.INVISIBLE);
			//btnLogin.setVisibility(View.INVISIBLE);
			//onlinestore.setVisibility(View.INVISIBLE);
			
			username=sp.getString("username", "");
			System.out.println("sessionid found");
			openWebView();
		}
		else{
			System.out.println("sessionid not found");			
		}
	}

	private void openWebView(){
		
		
		Intent i=new Intent(currentContext,MainFragment.class);
		//if(!hasSession){
			//i.putExtra("password", etPassword.getText().toString());
		//i.putExtra("username", username);
		startActivity(i);
		//}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	public class GetNewCSRF extends AsyncTask<String,Void,String>{
		@Override
		protected String doInBackground(String... arg0) {
			try{
				HttpParams httpParameters = new BasicHttpParams();
				HttpConnectionParams.setConnectionTimeout(httpParameters, 15000);
				HttpConnectionParams.setSoTimeout(httpParameters, 15000);
				HttpClient httpclient=new DefaultHttpClient(httpParameters);
				
				HttpGet getRequest=new HttpGet(URI.create(uri_login));
				
				HttpResponse getResponse=httpclient.execute(getRequest);
				
				System.out.println("GET CSRF status code:"+getResponse.getStatusLine().getStatusCode());
				List<Cookie> getCookies=((AbstractHttpClient)httpclient).getCookieStore().getCookies();
				getCSRFCookie=getCookies.get(0);
				
			}catch(Exception e){
				Log.d("GET CSRF Exception", e.toString());
			}
			return null;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			
			//if(MainActivity.hasSession){
		//		showWebView(webViewDash);
			//}
			//else{
				new LoginAsyncTask().execute("http://www.getmeashop.net/mobile/login/");
				
			//}
		}
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.show();
		}
		protected void onProgressUpdate(Integer...values){
			dialog.setProgress(values[0]);
		}
		

}
	private class LoginAsyncTask extends AsyncTask<String, Void, String> {
		
		@Override
		protected String doInBackground(String... urls) {
			return postRegisterDetails();
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			dialog.dismiss();

			if(!loginStatus){
				Toast.makeText(currentContext,"Login failed", Toast.LENGTH_SHORT).show();
			}

		}
		protected void onProgressUpdate(Integer...values){
			dialog.setProgress(values[0]);
		}
	}
	
//=====================postRegisterDetails========================================================
	private String postRegisterDetails()
	{	try {
		HttpParams httpParameters = new BasicHttpParams();

		//Setup timeouts
		HttpConnectionParams.setConnectionTimeout(httpParameters, 15000);
		HttpConnectionParams.setSoTimeout(httpParameters, 15000);			

		HttpClient httpclient = new DefaultHttpClient(httpParameters);

		((AbstractHttpClient)httpclient).getCookieStore().addCookie(getCSRFCookie);
		
		HttpPost httpPost = new HttpPost("http://www.getmeashop.net/mobile/login/");
/*		httpPost.addHeader("Host","www.getmeashop.org");
		httpPost.addHeader("User-Agent","Mozilla/5.0");
		httpPost.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,;q=0.8");
		httpPost.addHeader("Accept-Language" ,"en-US,en;q=0.5");
		httpPost.addHeader("Accept-Encoding", "gzip, deflate");
		httpPost.addHeader("Referer", "http://www.getmeashop.org/home/");
		httpPost.addHeader("Cookie", "csrftoken="+getCSRFCookie.getValue());
//ma=217405696.1532962016.1403170396.1403170396.1403170396.1; __utmb=217405696.1.10.1403170396; __utmc=217405696; __utmz=217405696.1403170396.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)");
		httpPost.addHeader("Connection", "keep-alive");
		httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
//        httpPost.addHeader("Content-Length", "93");
*/		
		Log.d("Cookie", getCSRFCookie.getValue());
		List<NameValuePair> nameValuePairs=new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("csrfmiddlewaretoken",getCSRFCookie.getValue()));
		nameValuePairs.add(new BasicNameValuePair("username",etUserName.getText().toString()));
		nameValuePairs.add(new BasicNameValuePair("password", etPassword.getText().toString()));
		Log.d("cookiecsrf", getCSRFCookie.getValue());
		httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		HttpResponse httpResponse = httpclient.execute(httpPost);
		List<Cookie> cookies=((AbstractHttpClient)httpclient).getCookieStore().getCookies();
		Utils.cookies=cookies;
		Log.d("tag", String.valueOf(cookies.size()));
		Log.d("tag", cookies.toString());
		for(Cookie c:cookies){
			cookieStore.addCookie(c);
			Log.d("tag" , c.getName());
//			if(c.getName().equals("sessionid")){
//				SharedPreferences.Editor editor=sp.edit();
//				editor.putString("username", username);.
//				editor.commit();
//			}
		}
		InputStream inputStream = httpResponse.getEntity().getContent();
		String responseString=Utils.convertInputStreamToString(inputStream);
		System.out.println("Login server response__"+responseString);
		
		JSONObject myObject = new JSONObject(responseString);
		String result = myObject.get("status").toString();
		if(result.equals("success"))
		{
			status =true;
			Intent i=new Intent(MainActivity.this,MainFragment.class);
			startActivity(i);
			loginStatus=true;
			hasSession=true;
			Log.d("tag","Login good");
			
		}
		
		System.out.println("login response:"+responseString);
	} catch (UnsupportedEncodingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (ClientProtocolException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (JSONException e) {
		loginStatus=false;
		e.printStackTrace();
	}
	catch (Exception e){
		e.printStackTrace();
	}


		return string_name;
		
	}
}