package com.harshit.gmasowner;

import java.util.ArrayList;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MySimpleSearchAdapter extends BaseAdapter {

    private ArrayList<String> mData = new ArrayList<String>();
    private ArrayList<String> mStock = new ArrayList<String>();
    private ArrayList<String> mImageID  = new ArrayList<String>();
    String str ;
    private LayoutInflater mInflater;

    public MySimpleSearchAdapter(Activity activity) {
        mInflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void addItems(String item,String stock, String imgurl) {
        mData.add(item);
        mStock.add(stock);
        mImageID.add(imgurl);
        
        notifyDataSetChanged();
    }
    

    public void addItem(String item) {
        mData.add(item);
        notifyDataSetChanged();
    }

    public int getCount() {
        return mData.size();
    }

    public String getItem(int position) {
        return mData.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.myproduct_item_view, null);
            holder.textPrice = (TextView)convertView.findViewById(R.id.item_price);
            holder.imgview = (ImageView)convertView.findViewById(R.id.item_img);
            holder.textView = (TextView) convertView.findViewById(R.id.item_txt);
            holder.share = (ImageView)convertView.findViewById(R.id.share);

    		holder.share.setImageResource(android.R.drawable.ic_menu_share);
            holder.share.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					CategoryFragment.shareApp(str, mStock.get(position), mImageID.get(position));
				}
			});
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        str = mData.get(position);
        UrlImageViewHelper.setUrlDrawable(holder.imgview,mImageID.get(position));
        holder.textView.setText("Name: " + str);
        holder.textPrice.setText("Price: " +mStock.get(position));
        return convertView;
    }

    public class ViewHolder {
    	public ImageView imgview;
        public TextView textPrice;
		public TextView textView;
		public ImageView share;
    }

}

