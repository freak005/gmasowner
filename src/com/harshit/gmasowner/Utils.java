package com.harshit.gmasowner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.cookie.Cookie;

public class Utils {
//	public static String uri_base="http://www.getmeashop.org/";
	public static List<Cookie> cookies=new ArrayList<Cookie>();
	public static String uri_base="http://www.getmeashop.net";
	public static String convertInputStreamToString(InputStream inputStream) throws IOException{
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null){
            result += line;
//            System.out.println("---->"+line);
        }
 
        inputStream.close();
        return result;
 
}
	
}
