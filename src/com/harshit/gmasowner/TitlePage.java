package com.harshit.gmasowner;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;

public class TitlePage extends Fragment {
	Button add_product, add_category, manage_product, manage_category, orders,
			notifications;

	ListView homeListView;
	@Override
	public View onCreateView(LayoutInflater inflater,
			final ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.title_page2, container, false);
		// View v = inflater.inflate(R.layout., root, attachToRoot)

		homeListView = (ListView) view.findViewById(R.id.lvHome);
		homeListView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View v,
					final int position, long id) {
				switch (position) {
				case 0:
					MainFragment.pager.setCurrentItem(1, true);
					break;
				case 1:
					MainFragment.pager.setCurrentItem(3, true);
					break;
				case 2:
					MainFragment.pager.setCurrentItem(2, true);
					break;
				case 3:
					MainFragment.pager.setCurrentItem(4, true);
					break;
				case 4:
//					MainFragment.pager.setCurrentItem(, true);
					break;
				case 5:
					MainFragment.pager.setCurrentItem(5, true);
					break;
				default:
					break;
				}
			}
		});

		/*
		add_product = (Button) view.findViewById(R.id.add_product);
		add_category = (Button) view.findViewById(R.id.add_category);
		manage_product = (Button) view.findViewById(R.id.manage_products);
		manage_category = (Button) view.findViewById(R.id.manage_category);
		orders = (Button) view.findViewById(R.id.Order);
		notifications = (Button) view.findViewById(R.id.notifications);

		add_product.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				MainFragment.pager.setCurrentItem(1, true);
			}
		});
		
		manage_category.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				MainFragment.pager.setCurrentItem(4, true);
			}
		});
		
		
		notifications.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				MainFragment.pager.setCurrentItem(5, true);
			}
		});



		add_category.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				MainFragment.pager.setCurrentItem(2, true);
			}
		});
		manage_product.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				MainFragment.pager.setCurrentItem(3, true);
			}
		});
		 */
		return view;
	}

}
