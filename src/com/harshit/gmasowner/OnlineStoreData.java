package com.harshit.gmasowner;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class OnlineStoreData extends Fragment {
	EditText name, phone, store;
	private ProgressDialog dialog;
	Button register;
	private String string_name, string_phone;
	public static Cookie getCSRFCookie;
	private boolean status = false;
	public Cookie contextCookie;
	public PersistentCookieStore cookieStore;
	SharedPreferences sp;
	ImageView imgv;
	public static String uri_login="http://www.getmeashop.net/mobile/login/", string_store;
	private String url = "http://www.getmeashop.net/mobile/registration/";
	private AddInfo ad;

	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		final View view = inflater
				.inflate(R.layout.onlinedatastore, container, false);
		name = (EditText) view.findViewById(R.id.name);
		phone = (EditText) view.findViewById(R.id.phone_num);
		store = (EditText) view.findViewById(R.id.store_name);
		imgv = (ImageView)view.findViewById(R.id.rightarrow);
		imgv.setVisibility(view.INVISIBLE);
		cookieStore=ad.cookieStore;
		dialog=new ProgressDialog(ad.context);
		dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	//	dialog.
		dialog.setMessage("Please wait...");
		dialog.setCancelable(false);
		register = (Button) view.findViewById(R.id.register);
		register.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				string_name = name.getText().toString();
				
				string_phone = phone.getText().toString();
				
				string_store = store.getText().toString();
				if(string_name.equals("") || string_phone.equals("") || string_store.equals("")){
					Toast.makeText(AddInfo.context,"Enter valid details", Toast.LENGTH_SHORT).show();
				}
				else
				{imgv.setVisibility(view.VISIBLE);
				new GetNewCSRF().execute(uri_login);
			}}
		});
		return view;
	}
//===========================LoginAsynTask==========================================================
	private class LoginAsyncTask extends AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... urls) {
			return postRegisterDetails();
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		
		protected void onPreExecute(){
			dialog.show();
			
		}
		protected void onPostExecute(String result) {
			dialog.dismiss();
			contextCookie =getCSRFCookie;
			if(status)
			{

				Toast.makeText(AddInfo.context,"Succesful Login", Toast.LENGTH_SHORT).show();
				AddInfo.pager.setCurrentItem(1);
			}
			
		}
		protected void onProgressUpdate(Integer...values){
			dialog.setProgress(values[0]);
		}
	}
	
//=====================postRegisterDetails========================================================
	private String postRegisterDetails()
	{	try {
		HttpParams httpParameters = new BasicHttpParams();

		//Setup timeouts
		HttpConnectionParams.setConnectionTimeout(httpParameters, 15000);
		HttpConnectionParams.setSoTimeout(httpParameters, 15000);			

		HttpClient httpclient = new DefaultHttpClient(httpParameters);

		((AbstractHttpClient)httpclient).getCookieStore().addCookie(getCSRFCookie);
		
		HttpPost httpPost = new HttpPost(url);
/*		httpPost.addHeader("Host","www.getmeashop.org");
		httpPost.addHeader("User-Agent","Mozilla/5.0");
		httpPost.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,;q=0.8");
		httpPost.addHeader("Accept-Language" ,"en-US,en;q=0.5");
		httpPost.addHeader("Accept-Encoding", "gzip, deflate");
		httpPost.addHeader("Referer", "http://www.getmeashop.org/home/");
		httpPost.addHeader("Cookie", "csrftoken="+getCSRFCookie.getValue());
//ma=217405696.1532962016.1403170396.1403170396.1403170396.1; __utmb=217405696.1.10.1403170396; __utmc=217405696; __utmz=217405696.1403170396.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)");
		httpPost.addHeader("Connection", "keep-alive");
		httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
//        httpPost.addHeader("Content-Length", "93");
*/		
		Log.d("Cookie", getCSRFCookie.getValue());
		List<NameValuePair> nameValuePairs=new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("csrfmiddlewaretoken",getCSRFCookie.getValue()));
		nameValuePairs.add(new BasicNameValuePair("name",string_name));
		nameValuePairs.add(new BasicNameValuePair("phone", string_phone));
		nameValuePairs.add(new BasicNameValuePair("storename", string_store));
		Log.d("ss", string_store);
		Log.d("sp", string_phone);
		Log.d("sn", string_name);
		Log.d("cookiecsrf", getCSRFCookie.getValue());
		httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		
		HttpResponse httpResponse = httpclient.execute(httpPost);
		List<Cookie> cookies=((AbstractHttpClient)httpclient).getCookieStore().getCookies();
		Log.d("tag", String.valueOf(cookies.size()));
		Log.d("tag", cookies.toString());
		for(Cookie c:cookies){
			cookieStore.addCookie(c);
			Log.d("tag" , c.getName());
//			if(c.getName().equals("sessionid")){
//				SharedPreferences.Editor editor=sp.edit();
//				editor.putString("username", username);.
//				editor.commit();
//			}
		}
		InputStream inputStream = httpResponse.getEntity().getContent();
		String responseString=Utils.convertInputStreamToString(inputStream);
		JSONObject myObject = new JSONObject(responseString);
		String result = myObject.get("status").toString();
		if(result.equals("success"))
		{
			status =true;

			Log.d("tag","Login good");
			
			
		}
		
		System.out.println("login response:"+responseString);
	} catch (UnsupportedEncodingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (ClientProtocolException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	catch (Exception e){
		e.printStackTrace();
	}


		return string_name;
		
	}
//================GetCsrfToken=============================================

	public class GetNewCSRF extends AsyncTask<String,Void,String>{
		@Override
		protected String doInBackground(String... arg0) {
			try{
				HttpParams httpParameters = new BasicHttpParams();
				HttpConnectionParams.setConnectionTimeout(httpParameters, 15000);
				HttpConnectionParams.setSoTimeout(httpParameters, 15000);
				HttpClient httpclient=new DefaultHttpClient(httpParameters);
				
				HttpGet getRequest=new HttpGet(URI.create(uri_login));
				
				HttpResponse getResponse=httpclient.execute(getRequest);
				
				System.out.println("GET CSRF status code:"+getResponse.getStatusLine().getStatusCode());
				List<Cookie> getCookies=((AbstractHttpClient)httpclient).getCookieStore().getCookies();
				getCSRFCookie=getCookies.get(0);
				
			}catch(Exception e){
				Log.d("GET CSRF Exception", e.toString());
			}
			return null;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			
			if(MainActivity.hasSession){
		//		showWebView(webViewDash);
			}
			else{
				new LoginAsyncTask().execute(url);
			}
		}
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
	//		dialog.show();
		}
		
	}
	
}
