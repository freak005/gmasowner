package com.harshit.gmasowner;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.harshit.gmasowner.database.DatabaseHandler;
import com.harshit.gmasowner.database.Products;


public class CategoryFragment extends Fragment {
	public static String uri_get_product_details;
	TextView textJson;
	private String[ ] web, price,imageId,stocks,pid,short_description, Id;
	List<Products> myProductList = new ArrayList<Products>();
	ListView list;
	private List<String> items;
	private List<String> itemsURL;
	private List<String> itemsStock;
	MySimpleSearchAdapter mAdapter;
	Button btnSearch, btnLeft;
	EditText mtxt;
	DatabaseHandler dbh ;
	
	List<Integer> searchPositions=new ArrayList<Integer>();
	
//	final static int[] items = new int[]  {R.drawable.images, R.drawable.gmail, R.drawable.whatsapp};
	Context currentContext;
	public void init(Context c){
		this.currentContext=c;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.frag_view_category, container,
				false);
		dbh = new DatabaseHandler(MainFragment.currentContext);
		uri_get_product_details="http://www.getmeashop.org/"+"admin"+"/api/data/product/?format=json";

		mAdapter = new MySimpleSearchAdapter((Activity)MainFragment.currentContext);
		btnSearch = (Button)view.findViewById(R.id.btnSearch);
		btnLeft = (Button)view. findViewById(R.id.btnLeft);
		mtxt = (EditText)view. findViewById(R.id.edSearch);
		list=(ListView)view.findViewById(R.id.listView_myProducts);
		
		myProductList = dbh.getAllContacts();
		showView();
//		
//		new GetProductAsyncTask().execute(uri_get_product_details);
		return view;
	}
	

	private void showView(){
		Id = new String[myProductList.size()];
		web=new String[myProductList.size()];
		imageId=new String[myProductList.size()];
		price= new String[myProductList.size()];
		stocks = new String[myProductList.size()];
		pid = new String[myProductList.size()];
		short_description = new String[myProductList.size()];
		
 		int i = 0;
		for (Products cn :myProductList) {
			String log = "Id: " + cn.getID() + " ,Name: " + cn.getName()
					+ " ,Phone: " + cn.getStock() + ",Price  " + cn.getPrice()
					+ ",InitialStock " + cn.getInitialStock()
					+  ",short description "
					+ cn.getShortDescription() + ", SYNCED  " + cn.getissynced();
			Id[i] = cn.getID();
			web[i] = cn.getName();
			stocks[i] = cn.getStock();
			imageId[i] = cn.getDescImage();
			pid[i] = cn.getInitialStock();
			short_description[i] = cn.getShortDescription();
			price[i] = cn.getPrice();

			i++;
			Log.d("Name: ", log);
		}
		mtxt.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				if (0 != mtxt.getText().length()) {
					String spnId = mtxt.getText().toString();
					setSearchResult(spnId);
				} else {
					setData();
				}
			}
		});
	//	btnLeft.setOnClickListener(this);
		//btnSearch.setOnClickListener(this);
		setData();

		list.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View v,
					final int pos, long id) {
				
				Intent intent = new Intent(MainFragment.currentContext, UpdateProduct.class);
				Bundle b=new Bundle();
				
				int position=searchPositions.get(pos);
				
				b.putString("id", Id[position]);
				b.putString("webid",web[position]);
				b.putString("imageid", imageId[position]);
				b.putString("price", price[position]);
				b.putString("stock", stocks[position]);
				b.putString("initial_stock", pid[position]);
				b.putString("sd", short_description[position]);
				intent.putExtras(b);
				startActivity(intent);
		/*		AlertDialog.Builder adb = new AlertDialog.Builder(
						MainFragment.currentContext);
				adb.setTitle("Current Stock");
				final EditText input = new EditText(MainFragment.currentContext);

				input.setText(price[position]);
				LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
						LinearLayout.LayoutParams.MATCH_PARENT,
						LinearLayout.LayoutParams.MATCH_PARENT);
				input.setLayoutParams(lp);
				adb.setView(input);
				
			*/
			}
		});

		ProductListView adapter = new
				ProductListView((Activity) MainFragment.currentContext, web, imageId,price);
		list.setAdapter(adapter);    
	}

	public void setSearchResult(String str) {
		mAdapter = new MySimpleSearchAdapter((Activity)MainFragment.currentContext);
		int i = 0;

		searchPositions.clear();
		for (String temp : items) {
			if (temp.toLowerCase().contains(str.toLowerCase())) {
				System.out.println("items: " + temp);
				mAdapter.addItems(temp, price[i], imageId[i]);
				Log.d("title", web[i]);
				Log.d("stock", price[i]);

				searchPositions.add(i);
			}
			i++;
		}
		// for(int i =0; i<items.size();i++)
		// {
		// if(web[0].toLowerCase().contains(str.toLowerCase()))
		// {
		// mAdapter.addItems(web[i],price[i],imageId[i]);
		// break;
		// }
		// }
		// }
		list.setAdapter(mAdapter);
	}

	public void setData() {
		items = new ArrayList<String>();
		itemsStock = new ArrayList<String>();
		itemsURL = new ArrayList<String>();

		searchPositions.clear();
		mAdapter = new MySimpleSearchAdapter((Activity)MainFragment.currentContext);
		for (int i = 0; i < web.length; i++) {
			mAdapter.addItems(web[i], price[i], imageId[i]);
			itemsStock.add(price[i]);
			itemsURL.add(imageId[i]);
			items.add(web[i]);
			searchPositions.add(i);
		}
//		list.setOnItemClickListener(this);
		list.setAdapter(mAdapter);
	}
	public static void shareApp(final String web ,final String price,final String image  ) {
	//	final AlertDialog.Builder builder = new AlertDialog.Builder(GmasActivity.currentContext);
	//	final Items[] items ={R.drawable.images, R.drawable.gmail, R.drawable.whatsapp};
		final Item[] items = {new Item("SMS", R.drawable.images),
			    new Item("Gmail", R.drawable.gmail),
			    new Item("Whatsapp",R.drawable.whatsapp ),
			    new Item("Others",0)//no icon for this one
			};
		ListAdapter adapter = new ArrayAdapter<Item>(
			    MainFragment.currentContext,
			    android.R.layout.select_dialog_item,
			    android.R.id.text1,
			    items){
			        public View getView(int position, View convertView, ViewGroup parent) {
			            //User super class to create the View
			            View v = super.getView(position, convertView, parent);
			            TextView tv = (TextView)v.findViewById(android.R.id.text1);

			            //Put the image on the TextView
			            tv.setCompoundDrawablesWithIntrinsicBounds(items[position].icon, 0, 0, 0);

			            //Add margin between image and text (support various screen densities)
			            int dp5 = (int) (5 * MainFragment.currentContext.getResources().getDisplayMetrics().density + 0.5f);
			            tv.setCompoundDrawablePadding(dp5);

			            return v;
			        }
			    };		
		
			    AlertDialog.Builder builder = new AlertDialog.Builder(MainFragment.currentContext);
			    builder.setTitle("Share via");
			   builder.setAdapter(adapter,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						if (which == 0) {
							Intent smsIntent = new Intent(Intent.ACTION_VIEW);
							smsIntent.setType("vnd.android-dir/mms-sms");
							smsIntent
									.putExtra(
											"sms_body","The price of " + web 
													+ " is" + price + " and is available at " +image );
							MainFragment.currentContext.startActivity(smsIntent);
						} else if (which == 1) {
							
							Intent email = new Intent(Intent.ACTION_SEND);
							email.putExtra(Intent.EXTRA_SUBJECT, "GetMeAShop");
							email.putExtra(
									Intent.EXTRA_TEXT,"The price of " + web 
									+ " is" + price + " and is available at " +image);

							// need this to prompts email client only
						email.setType("message/rfc822");

							MainFragment.currentContext.startActivity(Intent.createChooser(email,
									"Choose an Email client :"));
						} else if (which == 2) {
							Intent waIntent = new Intent(Intent.ACTION_SEND);
							waIntent.setType("text/plain");
							String text = "The price of  " + web 
									+ " is" + price + " and is available at " +image;
						waIntent.setPackage("com.whatsapp");
							if (waIntent != null) {
								waIntent.putExtra(Intent.EXTRA_TEXT, text);//
								MainFragment.currentContext.startActivity(Intent.createChooser(waIntent,
										"Share with"));
							}

						} else if (which == 3) {
							Intent shareIntent = new Intent(Intent.ACTION_SEND);
							shareIntent.setType("text/plain");
							shareIntent
									.putExtra(Intent.EXTRA_TEXT,
											"The price of " + web 
											+ " is" + price + " and is available at " +image);
							MainFragment.currentContext.startActivity(Intent.createChooser(shareIntent,
									"Share..."));
						}
					}
				}).show();
		
	}

	
//	private class ProductDetails{
//		String imageUri="";
//		String title="";
//		String price="";
//	}
//	
//	private class GetProductAsyncTask extends AsyncTask<String,Void,String>{
//
//		@Override
//		protected String doInBackground(String... arg0) {
//			// TODO Auto-generated method stub
//			try {
////				List<Cookie> savedCookies=cookieStore.getCookies();
//				HttpClient httpclient= new DefaultHttpClient();
////				if (savedCookies != null) {
////		            for (Cookie cookie:savedCookies) {
////		                ((AbstractHttpClient) httpclient).getCookieStore().addCookie(cookie);
////		            }
////		        }
//				HttpGet request=new HttpGet(URI.create(uri_get_product_details));
//				HttpResponse response= httpclient.execute(request);
//				InputStream inputStream  = response.getEntity().getContent();
//				String responseString= Utils.convertInputStreamToString(inputStream);
//				System.out.println(responseString);
//				JSONObject jsonResponse=new JSONObject(responseString);
//				JSONArray objects=jsonResponse.getJSONArray("objects");
//				for(int i=0;i<objects.length();i++){
//					JSONObject object=objects.getJSONObject(i);
//					ProductDetails product = new ProductDetails();
//					
//					product.imageUri=Utils.uri_base+object.getString("image");
//					product.title=object.getString("title");
//					product.price=object.getString("stock");
//					
//					myProductList.add(product);
//				}
//				System.out.println("THE JSON FROM RESPONSE:"+jsonResponse);
//			} catch (ClientProtocolException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (JSONException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//
//			return null;
//		}
//		@Override
//		protected void onPostExecute(String result){
//			showView();
//			System.out.println("Finishing my product details async task"); 
//		}
//	}
	public static class Item{
	    public final String text;
	    public final int icon;
	    public Item(String text, Integer icon) {
	        this.text = text;
	        this.icon = icon;
	    }
	    @Override
	    public String toString() {
	        return text;
	    }
	}
}

