package com.harshit.gmasowner;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CategoryList extends ArrayAdapter<String>{
	private final Activity context;
	private final String[] catname;
	private final String[] parname;
	public CategoryList(Activity context, String[] cat_name, String[] par_name) {

		super(context, R.layout.categories_list_view,cat_name);
		this.context = context;
		this.catname = cat_name;
		this.parname = par_name;
		
		// TODO Auto-generated constructor stub
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		LayoutInflater inflater = context.getLayoutInflater();
		View rowView= inflater.inflate(R.layout.categories_list_view, null, true);
		TextView priceText = (TextView) rowView.findViewById(R.id.item_catname);
		TextView txtTitle = (TextView) rowView.findViewById(R.id.item_parname); 
		priceText.setText("Name: " + catname[position]);
		txtTitle.setText("parent's category: " + parname[position]);
		return rowView;
	}

	


}
