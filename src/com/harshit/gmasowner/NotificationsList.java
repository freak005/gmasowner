package com.harshit.gmasowner;

import java.util.ArrayList;
import java.util.List;

import com.harshit.gmasowner.database.DatabaseHandler;
import com.harshit.gmasowner.database.Notifications;
import com.harshit.gmasowner.database.Products;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class NotificationsList extends Fragment {
	private ListView list;
	private String[ ] type, content,timestamp,email;
	DatabaseHandler dbh = new DatabaseHandler(MainFragment.currentContext);

	List<Notifications> myProductList = new ArrayList<Notifications>();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.notifications_list, container,false);

		dbh = new DatabaseHandler(MainFragment.currentContext);

		list=(ListView)view.findViewById(R.id.listView_myProducts);

		myProductList = dbh.getAllNotifications();
		showView();
		return view;
	}

	private void showView() {
		// TODO Auto-generated method stub
		type = new String[myProductList.size()];
		content=new String[myProductList.size()];
		timestamp=new String[myProductList.size()];
		email= new String[myProductList.size()];

 		int i = 0;
		for (Notifications cn :myProductList) {
			String log = "Id: " + cn.getID() + " ,Name: " + cn.getType()
					+ " ,Phone: " + cn.getEmail() + ",Price  " + cn.getContent()
					+ ",InitialStock " + cn.gettime();
			type[i] = cn.getType();
			content[i] = cn.getContent();
			timestamp[i] = cn.gettime();
			email[i] = cn.getEmail();

			i++;
			Log.d("Name: ", log);
		}
		NotificationsListView adapter = new
				NotificationsListView((Activity) MainFragment.currentContext, type, content,email,timestamp);
		list.setAdapter(adapter);    
	}
	}
	