package com.harshit.gmasowner;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.harshit.gmasowner.database.Categories;
import com.harshit.gmasowner.database.DatabaseHandler;

public class AddCategory extends Fragment implements OnItemSelectedListener {
	EditText category_name;
	Spinner spinner1;
	Button send;
	String string_category,string_parent_category;
	String label;
	DatabaseHandler dbh;
	List<Categories> myProductList = new ArrayList<Categories>();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view  =inflater.inflate(R.layout.add_category, container, false);
		category_name = (EditText)view.findViewById(R.id.categoryname);
		spinner1= (Spinner)view.findViewById(R.id.parentcategory);
		loadSpinnerData();
		send = (Button)view.findViewById(R.id.send);
		dbh = new DatabaseHandler(MainFragment.currentContext);
		send.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				label = spinner1.getSelectedItem().toString();
				// TODO Auto-generated method stub
				string_category = category_name.getText().toString();
				List<String> catnames=dbh.getAllCategoryNames();
				System.out.println(catnames);
				Log.d("label selected", label);
				
				if(catnames.contains(string_category)){
					Toast.makeText(MainFragment.currentContext, "Duplicate entry", Toast.LENGTH_SHORT).show();
				}
				else{
					if(label.equals("Select Parent Category"))
					{	
						
						Toast.makeText(MainFragment.currentContext, "Please Select a Category", Toast.LENGTH_SHORT).show();
					}
					else
					{
					if(label.equals("null"))
					{
						string_parent_category="null";
					}
					
					else{
						string_parent_category=dbh.getIdFromCatName(label);
					}
					Log.d("parent id", string_parent_category);
					dbh.addcategory(new Categories("-1", string_category, string_parent_category,"false"));
					
					Intent intent = new Intent (MainFragment.currentContext,MainFragment.class);
					startActivity(intent);

					}
				}
			}
		});
		return view;
	}
	 private void loadSpinnerData() {
	        // database handler
	        DatabaseHandler dbh = new DatabaseHandler(MainFragment.currentContext);
	 
	        // Spinner Drop down elements
	        List<String> lables = dbh.getAllCategoryIDs();
	        lables.add("null");
	        lables.add("Select Parent Category");
	 
	        // Creating adapter for spinner
//	        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
//	                android.R.layout.simple_spinner_item, lables);
	        ArrayAdapter<String>dataAdapter = new ArrayAdapter<String>(MainFragment.currentContext, android.R.layout.simple_spinner_item,lables);
	 
	        // Drop down layout style - list view with radio button
	        dataAdapter
	                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	 
	        // attaching data adapter to spinner
	        spinner1.setAdapter(dataAdapter);
	        spinner1.setSelection(lables.size()-1);
	    }
	 
	    public void onItemSelected(AdapterView<?> parent, View view, int position,
	            long id) {
	        // On selecting a spinner item
//	 /        label = parent.getItemAtPosition(position).toString();
	 
	        // Showing selected spinner item
	        Toast.makeText(parent.getContext(), "You selected: " + label,
	                Toast.LENGTH_LONG).show();
	 
	    }
	 
	    public void onNothingSelected(AdapterView<?> arg0) {
	        // TODO Auto-generated method stub
	 
	    }

}
