package com.harshit.gmasowner;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Interpolator;

import com.viewpagerindicator.CirclePageIndicator;

public class AddInfo extends FragmentActivity {
	MyPageAdapter pageAdapter;
	static ViewPager pager;
	static Context context;

	CirclePageIndicator mIndicator;

	public static PersistentCookieStore cookieStore;

	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		requestWindowFeature(Window.FEATURE_NO_TITLE); 
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.addinfo);
		Interpolator sInterpolator = new AccelerateInterpolator();
		context=this;
		cookieStore=new PersistentCookieStore(this.getApplicationContext());
		pageAdapter = new MyPageAdapter(getSupportFragmentManager());
		pageAdapter.addonlinedatainfo();
		pageAdapter.addItem();
		pager = (ViewPager) findViewById(R.id.pager_addinfo);
		mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
	    
		pager.setOffscreenPageLimit(3);
		pager.setAdapter(pageAdapter);
		mIndicator.setViewPager(pager);
		try {
		    Field mScroller;
		    mScroller = ViewPager.class.getDeclaredField("mScroller");
		    mScroller.setAccessible(true); 
		    FixedSpeedScroller scroller = new FixedSpeedScroller(pager.getContext(), sInterpolator);
		    // scroller.setFixedDuration(5000);
		    mScroller.set(pager, scroller);
		} catch (NoSuchFieldException e) {
		} catch (IllegalArgumentException e) {
		} catch (IllegalAccessException e) {
		}
	}
//==================================================================================
	class MyPageAdapter extends FragmentPagerAdapter {
		private List<Fragment> fragments;
		private List<String> titles;

		public MyPageAdapter(FragmentManager fm) {
			super(fm);
			this.fragments = new ArrayList<Fragment>();
			this.titles = new ArrayList<String>();
		}

		public void addItem() {
			
		  Fragment myFragment = new AddProduct(); 
			 Bundle args = new Bundle(); 
		 
			 this.fragments.add(myFragment);
			
			 
		}
		 public void addonlinedatainfo()
		 {
			 Fragment myFragmentss = new OnlineStoreData(); 
			 Bundle args = new Bundle(); 
		 
			 this.fragments.add(myFragmentss);;
		
		 }

		@Override
		public Fragment getItem(int position) {
			return this.fragments.get(position);
		}

		public CharSequence getPageTitle(int position) {
			return this.titles.get(position);
		}

		@Override
		public int getCount() {
			return this.fragments.size();
		}
	}
}
