package com.harshit.gmasowner;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.spec.EncodedKeySpec;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class AddProduct extends Fragment{
	EditText addproduct,price,stock;
	ImageView image;
	Button sendproduct;
	private AddInfo adi;
	private String string_price,string_addproduct,string_stock,username;
	private static final int CAMERA_REQUEST = 1888; 
	private String url = "http://www.getmeashop.net/mobile/product/";
	private Cookie getCSRFCookie;
	public PersistentCookieStore cookieStore;
	private String encodedImage;

	private ProgressDialog dialog;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub		
		View view = inflater.inflate(R.layout.addproducts_register, container, false);
		Bundle b = getArguments();
		
		
//		cookieStore = adi.cookieStore;
		addproduct = (EditText)view.findViewById(R.id.addproduct);
		
		price = (EditText)view.findViewById(R.id.price);
		sendproduct =(Button)view.findViewById(R.id.sendproduct);
		stock = (EditText)view.findViewById(R.id.stock);
		image = (ImageView)view.findViewById(R.id.imageView1);
		
		image.setOnClickListener(new View.OnClickListener() {

	            @Override
	            public void onClick(View v) {
	                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE); 
	                startActivityForResult(cameraIntent, CAMERA_REQUEST); 
	            }
	        });
	    
		sendproduct.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				string_addproduct = addproduct.getText().toString();
				string_price = price.getText().toString();
				string_stock = stock.getText().toString();
				new LoginAsyncTask().execute(url);
				
			}
		});
		dialog = new ProgressDialog(adi.context);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage("Please wait...");
        dialog.setCancelable(false);
		return view;
	}
	
	//===========================LoginAsynTask==========================================================
		private class LoginAsyncTask extends AsyncTask<String, Void, String> {
			@Override
			protected String doInBackground(String... urls) {
				
				return postRegisterDetails();
			}
			
			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				dialog.show();
				cookieStore=AddInfo.cookieStore;
				username = OnlineStoreData.string_store;
				getCSRFCookie = OnlineStoreData.getCSRFCookie;
			}

			// onPostExecute displays the results of the AsyncTask.
			@Override
			protected void onPostExecute(String result) {
				dialog.dismiss();
				adi.finish();
//				Intent intent = new Intent(AddInfo.context, MainActivity.class);
//				startActivity(intent);
				
			}
			protected void onProgressUpdate(Integer... values){
				dialog.setProgress(values[0]);
			}
		}
		
	//=====================postRegisterDetails========================================================
		private String postRegisterDetails()
		{	try {
			HttpParams httpParameters = new BasicHttpParams();

			//Setup timeouts
			HttpConnectionParams.setConnectionTimeout(httpParameters, 15000);
			HttpConnectionParams.setSoTimeout(httpParameters, 15000);			

			HttpClient httpclient = new DefaultHttpClient(httpParameters);

			((AbstractHttpClient)httpclient).getCookieStore().addCookie(getCSRFCookie);
			
			HttpPost httpPost = new HttpPost(url);
	/*		httpPost.addHeader("Host","www.getmeashop.org");
			httpPost.addHeader("User-Agent","Mozilla/5.0");
			httpPost.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,;q=0.8");
			httpPost.addHeader("Accept-Language" ,"en-US,en;q=0.5");
			httpPost.addHeader("Accept-Encoding", "gzip, deflate");
			httpPost.addHeader("Referer", "http://www.getmeashop.org/home/");
			httpPost.addHeader("Cookie", "csrftoken="+getCSRFCookie.getValue());
	//ma=217405696.1532962016.1403170396.1403170396.1403170396.1; __utmb=217405696.1.10.1403170396; __utmc=217405696; __utmz=217405696.1403170396.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)");
			httpPost.addHeader("Connection", "keep-alive");
			httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
//	        httpPost.addHeader("Content-Length", "93");
	*/		
			Log.d("Cookie", getCSRFCookie.getValue());
			List<NameValuePair> nameValuePairs=new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("csrfmiddlewaretoken",getCSRFCookie.getValue()));
			nameValuePairs.add(new BasicNameValuePair("title",string_addproduct));
			nameValuePairs.add(new BasicNameValuePair("price", string_price));
			nameValuePairs.add(new BasicNameValuePair("stock", string_stock));
			nameValuePairs.add(new BasicNameValuePair("image", encodedImage));
			nameValuePairs.add(new BasicNameValuePair("username", username));
			Log.d("ss", string_addproduct);
			Log.d("username", username);
			Log.d("sp", string_price);
			Log.d("sn", string_stock);
			Log.d("cookiecsrf", getCSRFCookie.getValue());
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			
			HttpResponse httpResponse = httpclient.execute(httpPost);
			List<Cookie> cookies=((AbstractHttpClient)httpclient).getCookieStore().getCookies();
			System.out.println("cookie: "+cookies.toString());
			System.out.println("cookie size: "+String.valueOf(cookies.size()));
			Log.d("tag", String.valueOf(cookies.size()));
			Log.d("tag", cookies.toString());
			for(Cookie c:cookies){
				cookieStore.addCookie(c);
				Log.d("tag" , c.getName());
//				if(c.getName().equals("sessionid")){
//					SharedPreferences.Editor editor=sp.edit();
//					editor.putString("username", username);
//					editor.commit();
//				}
			}
			InputStream inputStream = httpResponse.getEntity().getContent();
			String responseString=Utils.convertInputStreamToString(inputStream);
			
			System.out.println("login response:"+responseString);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


			return "success";
			
		}
	 public void onActivityResult(int requestCode, int resultCode, Intent data) {  
	        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {  
	            Bitmap photo = (Bitmap) data.getExtras().get("data"); 
	            image.setImageBitmap(photo);
	            ByteArrayOutputStream baos = new ByteArrayOutputStream();
	            photo.compress(Bitmap.CompressFormat.JPEG, 100, baos);
	            byte[] imageBytes = baos.toByteArray();
	            encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
	            
	        } 
	
	
	 }
}
