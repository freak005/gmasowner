package com.harshit.gmasowner;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

public class ProductListView extends ArrayAdapter<String>{
	private final Activity context;
	private final String[] web;
	private final String[] imageId;
	private final String[] price;
	
	public ProductListView(Activity context,String[] web, String[] imageId,String[] price) {
		super(context, R.layout.myproduct_item_view, web);
		this.context = context;
		this.web = web;
		this.imageId = imageId;
		this.price = price;
	}
	
	@Override
	public View getView(final int position, View view, ViewGroup parent) {
		LayoutInflater inflater = context.getLayoutInflater();
		View rowView= inflater.inflate(R.layout.myproduct_item_view, null, true);
	/*	LinearLayout ll = (LinearLayout)rowView.findViewById(R.id.ll);
		ll.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.d("price", price[position]);
			}
		});*/
		TextView priceText = (TextView) rowView.findViewById(R.id.item_price);
		TextView txtTitle = (TextView) rowView.findViewById(R.id.item_txt); 
		
		ImageView imageView = (ImageView) rowView.findViewById(R.id.item_img);
		ImageView share = (ImageView)rowView.findViewById(R.id.share);
		share.setImageResource(android.R.drawable.ic_menu_share);
	share.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				CategoryFragment.shareApp(web[position], price[position], imageId[position]);
			}
		});
/*		imageView.setWebViewClient(new WebViewClient(){

			@Override
			public void onPageFinished(WebView view, String url) {
			System.out.println("image url loaded:"+url);
				super.onPageFinished(view, url);
			}
			
		});*/
/*		
		imageView.getSettings().setJavaScriptEnabled(true);
		imageView.getSettings().setAppCacheEnabled(true);
*/		if(imageId[position].length()>100)
	{
	  byte[] decodedByte = Base64.decode(imageId[position], 0);
	   Bitmap image = BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length); 
	   
	imageView.setImageBitmap(image);
	}
else if(imageId[position].equals("http://www.getmeashop.org/null")){

		imageView.setImageResource(R.drawable.product);
	}else if(imageId[position].contains("media"))
	{
		UrlImageViewHelper.setUrlDrawable(imageView, imageId[position]);
	}
		txtTitle.setText("Name: " + web[position]);
		priceText.setText("Price: " + price[position]);
//		imageView.loadUrl(imageId[position]);
//		imageView.setImageResource(imageId[position]);
		return rowView;
	}

	
}