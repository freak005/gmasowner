package com.harshit.gmasowner;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.harshit.gmasowner.database.Categories;
import com.harshit.gmasowner.database.DatabaseHandler;
import com.harshit.gmasowner.database.Products;

public class AddProducts extends Fragment {
	EditText name,stock, price, short_description, initial_stock;
	Spinner category;
	Button addproduct;

	private String encodedImage;
	ImageView imgview;
	DatabaseHandler dbh;
	private Toast toast = null;

	List<Categories> myProductList = new ArrayList<Categories>();
	String string_name, string_stock,string_price,string_short_description,string_initial_request;
	

	private static final int CAMERA_REQUEST = 1888; 

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.addproduct, container, false);
		dbh = new DatabaseHandler(MainFragment.currentContext);
		name= (EditText)view.findViewById(R.id.addproduct);
		stock = (EditText)view.findViewById(R.id.stock);
		price = (EditText)view.findViewById(R.id.price);
		short_description = (EditText)view.findViewById(R.id.short_decription);
		initial_stock = (EditText)view.findViewById(R.id.initial_stock);
		category = (Spinner)view.findViewById(R.id.category);
		loadSpinnerData();
		imgview = (ImageView)view.findViewById(R.id.imageView1);
		encodedImage = "www.getmeashop.org/null";
		imgview.setOnClickListener(new View.OnClickListener() {

	            @Override
	            public void onClick(View v) {
	                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE); 
	                startActivityForResult(cameraIntent, CAMERA_REQUEST); 
	            }
	        });
		addproduct = (Button)view.findViewById(R.id.sendproduct);
		addproduct.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String cat=null;
				// TODO Auto-generated method stub
				if(category.getSelectedItem()!=null)
				cat = category.getSelectedItem().toString();
				String catid = dbh.getIdFromCatName(cat);
				string_name = name.getText().toString();
				string_stock = stock.getText().toString();
				string_price = price.getText().toString();
				string_short_description = short_description.getText().toString();
				string_initial_request = initial_stock.getText().toString();
				if(string_stock.equals("") || string_initial_request.equals(""))
				{
			showToast("Please fill your information correctly");
				}
				else
				{
				if(dbh.checkDuplicateProduct(string_name+string_price))
				{
					showToast("Duplicate Entry");
				}
				else
				{
					
				
				dbh.addallproduct(new Products("-1",
						string_name, string_stock,string_price,string_initial_request, string_short_description, encodedImage, catid,"false"));
				showToast("Successfully added");
				Intent intent = new Intent (MainFragment.currentContext,MainFragment.class);
				startActivity(intent);
				}
			}
			}
		});
		
		return view;
	}
	
	private void showToast(String message) {
		if (this.toast == null) {
			// Create toast if found null, it would he the case of first call
			// only
			this.toast = Toast.makeText(MainFragment.currentContext, message, Toast.LENGTH_SHORT);

		} else if (this.toast.getView() == null) {
			// Toast not showing, so create new one
			this.toast = Toast.makeText(MainFragment.currentContext, message, Toast.LENGTH_SHORT);

		} else {
			// Updating toast message is showing
			this.toast.setText(message);
		}

		// Showing toast finally
		this.toast.show();
	}
	
	private void loadSpinnerData() {
        // database handler
        DatabaseHandler dbh = new DatabaseHandler(MainFragment.currentContext);
 
        // Spinner Drop down elements
        List<String> lables = dbh.getAllCategoryIDs();
        lables.add("null");
        lables.add("Select Parent Category");
        
        // Creating adapter for spinner
//        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
//                android.R.layout.simple_spinner_item, lables);
        ArrayAdapter<String>dataAdapter = new ArrayAdapter<String>(MainFragment.currentContext, android.R.layout.simple_spinner_item,lables);
 
        // Drop down layout style - list view with radio button
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
 
        // attaching data adapter to spinner
        category.setAdapter(dataAdapter);
        category.setSelection(lables.size() -1);
    }
 
    public void onItemSelected(AdapterView<?> parent, View view, int position,
            long id) {
        // On selecting a spinner item
        String label = parent.getItemAtPosition(position).toString();
 
        // Showing selected spinner item
        Toast.makeText(parent.getContext(), "You selected: " + label,
                Toast.LENGTH_LONG).show();
 
    }
 
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
 
    }

	
	 public void onActivityResult(int requestCode, int resultCode, Intent data) {  
	        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {  
	            Bitmap photo = (Bitmap) data.getExtras().get("data"); 
	            imgview.setImageBitmap(photo);
	            
	            ByteArrayOutputStream baos = new ByteArrayOutputStream();
	            photo.compress(Bitmap.CompressFormat.JPEG, 100, baos);
	            byte[] imageBytes = baos.toByteArray();
	            encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
	            
	        } 
	
	
	 }

}
