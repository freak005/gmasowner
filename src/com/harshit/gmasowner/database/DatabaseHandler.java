package com.harshit.gmasowner.database;

import java.util.ArrayList;
import java.util.List;

import com.crashlytics.android.internal.p;
import com.harshit.gmasowner.MainActivity;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper {
	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 5;

	// Database Name
	private static final String DATABASE_NAME = "productsmanager";
	static Context context;

	// table name
	private static final String TABLE_PRODUCTS = "products";
	private static final String TABLE_CATEGORIES = "categories";
	private static final String TABLE_NOTIFICATIONS = "notifications";

	// Products Table Columns names
	private static final String KEY_ID = "id";
	private static final String KEY_TITLE = "title";
	private static final String KEY_STOCK = "stock";
	private static final String KEY_INITIAL_STOCK = "initial_stock";
	private static final String KEY_PRICE = "price";
	private static final String KEY_SHORT_DESCRIPTION = "short_description";
	private static final String KEY_DESC_IMAGE = "desc_image";
	private static final String KEY_PRODUCTID = "productid";
	private static final String KEY_CATID = "catid";
	private static final String KEY_ISSYNCED = "sync";

	// Categories Table Columns Name
	private static final String KEYID = "_id";
	private static final String KEY_CAT_ID = "cat_id";
	private static final String KEY_CAT_NAME = "cat_name";
	private static final String KEY_PARENT_ID = "parent_id";
	private static final String KEY_PARENT_NAME = "parent_name";
	private static final String KEY_CAT_SYNCED = "cat_sync";

	// Notifiacations Table Coulumns Name
	private static final String KEY_NID = "nid";
	private static final String KEY_N_ID = "n_id";
	private static final String KEY_N_TYPE = "type";
	private static final String KEY_EMAIL = "email";
	private static final String KEY_CONTENT = "content";
	private static final String KEY_READ = "read";

	public static int productCounter=0;
	public static int categoryCounter=0;
	
	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.context = context;
		
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_PRODUCTS + "("
				+ KEY_ID + " INTEGER PRIMARY KEY," 
				+ KEY_PRODUCTID + " TEXT,"
				+ KEY_TITLE + " TEXT," 
				+ KEY_STOCK + " TEXT," 
				+ KEY_PRICE + " TEXT," 
				+ KEY_INITIAL_STOCK + " TEXT,"
				+ KEY_SHORT_DESCRIPTION + " TEXT," 
				+ KEY_DESC_IMAGE + " TEXT,"
				+ KEY_CATID + " TEXT," 
				+ KEY_ISSYNCED + " TEXT" + ")";
		String CREATE_CATEGORIES_TABLE = "CREATE TABLE " + TABLE_CATEGORIES + "(" 
				+ KEYID + " INTEGER PRIMARY KEY," 
				+ KEY_CAT_ID + " TEXT,"
				+ KEY_CAT_NAME + " TEXT," 
				+ KEY_PARENT_ID + " TEXT, "
				+ KEY_CAT_SYNCED + " TEXT" + ")";
		String CREATE_NOTIFICATION_TABLE = "CREATE TABLE " + TABLE_NOTIFICATIONS
				+ "(" + KEY_NID + " INTEGER PRIMARY KEY," + KEY_N_ID + " TEXT,"
				+ KEY_N_TYPE + " TEXT," + KEY_EMAIL + " TEXT, "+ KEY_CONTENT + " TEXT, "
				+ KEY_READ + " TEXT" + ")";
		db.execSQL(CREATE_CONTACTS_TABLE);
		db.execSQL(CREATE_CATEGORIES_TABLE);
		db.execSQL(CREATE_NOTIFICATION_TABLE);
		Log.d("dbh", "creating dbh");

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCTS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORIES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTIFICATIONS);

		// Create tables again
		onCreate(db);

	}

	public void deletedatabase() {
		Log.d("Database name", DATABASE_NAME);
		context.deleteDatabase(DATABASE_NAME);
	}
//================================Notifications Database============================================================	
	public void addallNotifications(Notifications notification) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_N_ID,notification.getID());
		values.put(KEY_N_TYPE, notification.getType());
		values.put(KEY_EMAIL, notification.getEmail());
		values.put(KEY_CONTENT, notification.getContent());
		values.put(KEY_READ, notification.getRead());
		Log.d("email", notification.getEmail());
		Log.d("all added", "notifications added successfully");
		// Inserting Row1
		db.insert(TABLE_NOTIFICATIONS, null, values);
		db.close(); // Closing database connection
	}
	
	public List<Notifications> getAllNotifications() {
		List<Notifications> productList = new ArrayList<Notifications>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_NOTIFICATIONS;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		int i = 0;
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Notifications n = new Notifications();
				n.setID(cursor.getString(1));
				n.settype(cursor.getString(2));
				n.setEmail(cursor.getString(3));
				n.setContent(cursor.getString(4));
				n.setRead(cursor.getString(5));
				// Adding contact to list
				productList.add(n);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		// return contact list
		return productList;
	}
	
	
	
//================================Product Database===========================================================
	public void addproduct(Products product) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_TITLE, product.getName()); // Contact Name
		values.put(KEY_STOCK, product.getStock()); // Contact Phone

		// Inserting Row
		db.insert(TABLE_PRODUCTS, null, values);
		db.close(); // Closing database connection
	}

	public int updateSingleProduct(Products product) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_PRODUCTID, product.getID());
		values.put(KEY_PRICE, product.getPrice());
		values.put(KEY_DESC_IMAGE, product.getDescImage());
		values.put(KEY_ISSYNCED, product.getissynced());
		
		// updating row
		return db.update(TABLE_PRODUCTS, values, KEY_TITLE + " = ?",
				new String[] { String.valueOf(product.getName()) });

	}

	// Updating single contact
	public int updateProduct(Products product) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_TITLE, product.getName()); // Contact Name
		values.put(KEY_STOCK, product.getStock());
		values.put(KEY_PRICE, product.getPrice());
		values.put(KEY_INITIAL_STOCK, product.getInitialStock());
		values.put(KEY_SHORT_DESCRIPTION, product.getShortDescription());
		values.put(KEY_DESC_IMAGE, product.getDescImage());
		values.put(KEY_CATID, product.getCATID());
		values.put(KEY_ISSYNCED, product.getissynced());
		Log.d("all added", product.getShortDescription());

		List<Products> p=getAllContacts();
		System.out.println("BEFORE:");
		for(int i=0;i<p.size();i++){
			System.out.println(p.get(i)._title+"-"+p.get(i)._id);
		}
		// updating row
		int ret = db.update(TABLE_PRODUCTS, values, KEY_PRODUCTID + " = ?",
				new String[] { String.valueOf(product.getID()) });
	
		p=getAllContacts();
		System.out.println("AFTER:");
		for(int i=0;i<p.size();i++){
			System.out.println(p.get(i)._title+"-"+p.get(i)._id);
		}
		db.close();
		return ret;
	}

	public void addallproduct(Products product) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_PRODUCTID, productCounter++);
		values.put(KEY_TITLE, product.getName()); // Contact Name
		values.put(KEY_STOCK, product.getStock());
		values.put(KEY_PRICE, product.getPrice());
		values.put(KEY_INITIAL_STOCK, product.getInitialStock());
		values.put(KEY_SHORT_DESCRIPTION, product.getShortDescription());
		values.put(KEY_DESC_IMAGE, product.getDescImage());
		values.put(KEY_ISSYNCED, product.getissynced());
		values.put(KEY_CATID, product.getCATID());
		Log.d("all added", "products added successfully");
		// Inserting Row1
		db.insert(TABLE_PRODUCTS, null, values);
		db.close(); // Closing database connection
		
		MainActivity.dbPrefs.edit().putInt("productCounter", productCounter).apply();
	}

	public boolean checkDuplicateProduct(String check) {
		SQLiteDatabase db = this.getWritableDatabase();
		boolean answer = false;
		String selectQuery = "SELECT  * FROM " + TABLE_PRODUCTS;
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				String duplicate = cursor.getString(2) + cursor.getString(4);
				if (check.equals(duplicate)) {
					answer = true;
				}
			} while (cursor.moveToNext());

		}
		cursor.close();
		db.close();
		return answer;
	}
	public boolean errorsync(){
		boolean shouldsync =false;
		String selectQuery = "SELECT  * FROM " + TABLE_PRODUCTS;
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
					if(cursor.getString(9).equals("false"))
					{
						shouldsync = true;
						break;
					}
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		return shouldsync;
		
	}
	public List<Products> getAllContacts() {
		List<Products> productList = new ArrayList<Products>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_PRODUCTS;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		int i = 0;
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Products products = new Products();
				products.setID(cursor.getString(1));
				products.setName(cursor.getString(2));
				products.setStock(cursor.getString(3));
				products.setPrice(cursor.getString(4));
				products.setInitialStock(cursor.getString(5));
				products.setShortDescription(cursor.getString(6));
				products.setDescImage(cursor.getString(7));
				products.setissynced(cursor.getString(9));
				// Adding contact to list
				productList.add(products);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		// return contact list
		return productList;
	}

	public Products getProducts(String id) {
		String selectQuery = "SELECT  * FROM " + TABLE_PRODUCTS + " WHERE "
				+ KEY_ID + " = " + id;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		cursor.moveToFirst();
		Products product = new Products();
		if (!cursor.moveToNext()) {
			product.setID(cursor.getString(2));

		}
		cursor.close();
		db.close();
		return product;

	}

	public void removeAll() {

		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("delete from " + TABLE_PRODUCTS);
	}

	public List<String> getAllStyleIDs() {
		List<String> results = new ArrayList<String>();
		SQLiteDatabase db = this.getReadableDatabase();
		// Select All Query
		String selectQuery = "select * from " + TABLE_PRODUCTS;

		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {

				// results.add(cursor.getInt(0));
				results.add(cursor.getString(2));

			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();

		return results;
	}

	public List<Products> getNotSyncedProducts() {
		List<Products> productList = new ArrayList<Products>();
		// Select All Query
		String selectQuery = "SELECT *  FROM " + TABLE_PRODUCTS + " WHERE "
				+ KEY_ISSYNCED + " = 'false'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		int i = 0;
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Products products = new Products();
				products.setID(cursor.getString(1));
				products.setName(cursor.getString(2));
				products.setStock(cursor.getString(3));
				products.setPrice(cursor.getString(4));
				products.setInitialStock(cursor.getString(5));
				products.setShortDescription(cursor.getString(6));
				products.setDescImage(cursor.getString(7));
				products.setCATID(cursor.getString(8));
				products.setissynced(cursor.getString(9));
				// Adding contact to list
				productList.add(products);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		// return contact list
		return productList;
	}
	
	
//========================Category Database========================================================================	

	// Updating single contact
	public int updateCategory(Categories category) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_CAT_NAME, category.getname()); // Contact Name
		values.put(KEY_PARENT_ID, category.getparentid());
		values.put(KEY_CAT_SYNCED, category.getissynced());

		// updating row
		return db.update(TABLE_CATEGORIES, values, KEY_CAT_ID + " = ?",
				new String[] { String.valueOf(category.getID()) });
	}

	public List<Categories> getNotSyncedCategory() {
		List<Categories> productList = new ArrayList<Categories>();
		// Select All Query
		String selectQuery = "SELECT *  FROM " + TABLE_CATEGORIES + " WHERE "
				+ KEY_CAT_SYNCED + " = 'false'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		int i = 0;
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Categories category = new Categories();
				category.setId(cursor.getString(1));
				category.setname(cursor.getString(2));
				category.setparentid(cursor.getString(3));
				// Adding contact to list
				productList.add(category);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		// return contact list
		return productList;
	}

	public void addcategory(Categories category) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
	//	values.put(KEY_CAT_ID, categoryCounter++); // Contact Name
		values.put(KEY_CAT_ID, category.getID());
		values.put(KEY_CAT_NAME, category.getname()); // Contact Phone
		values.put(KEY_PARENT_ID, category.getparentid());
		values.put(KEY_CAT_SYNCED, category.getissynced());
		// Inserting Row
		db.insert(TABLE_CATEGORIES, null, values);
		db.close(); // Closing database connection
		
		MainActivity.dbPrefs.edit().putInt("categoryCounter", categoryCounter).apply();

	}
	public boolean errorsynccategory(){
		boolean shouldsync =false;
		String selectQuery = "SELECT  * FROM " + TABLE_CATEGORIES;
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
					if(cursor.getString(4).equals("false"))
					{
						shouldsync = true;
						break;
					}
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		return shouldsync;
		
	}
	public List<Categories> getAllCategory() {
		List<Categories> categorylist = new ArrayList<Categories>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_CATEGORIES;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		int i = 0;
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Categories products = new Categories();
				products.setId(cursor.getString(1));
				products.setname(cursor.getString(2));
				products.setparentid(cursor.getString(3));
				products.setissynced(cursor.getString(4));
				Log.d("id" + " name" + " parentid", cursor.getString(1) + " "
						+ cursor.getString(2) + " " + cursor.getString(3));
				// Adding category to list
				categorylist.add(products);
			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();
		// return contact list
		return categorylist;
	}

	public void removeCategory() {

		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("delete from " + TABLE_CATEGORIES);
	}

	public String getIdFromCatName(String name) {
		// Log.d("")
		String selectQuery = "SELECT *  FROM " + TABLE_CATEGORIES;
		// + " WHERE "
		// + KEY_CAT_NAME + " = " + name;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		cursor.moveToFirst();
		String id = null;
		// Categories product = new Categories();
		// if(cursor.getCount()>0)
		// {
		// if (cursor.getString(2).equals(name)) {
		// id = cursor.getString(1);
		//
		// }
		// }

		if (cursor.moveToFirst())
			do {
				{
					if (cursor.getString(2).equals(name)) {
						id = cursor.getString(1);

					}
				}
			} while (cursor.moveToNext());
		cursor.close();
		db.close();
		return id;

	}

	public String getCatNameFromId(String id) {
		String selectQuery = "SELECT *  FROM " + TABLE_CATEGORIES + " WHERE "
				+ KEY_CAT_ID + " = " + id;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		String name = null;
		cursor.moveToFirst();
		Log.d("count" , String.valueOf(cursor.getCount()));
		// Categories product = new Categories();
		if (cursor.getCount() > 0) {
			Log.d("before if", cursor.getString(2));
			if (cursor.getString(1).equals(id)) {
				Log.d("testing", cursor.getString(2));
				name = cursor.getString(2);

			}
		}
		cursor.close();
		db.close();
		return name;

	}

	public List<String> getAllCategoryIDs() {
		List<String> results = new ArrayList<String>();
		SQLiteDatabase db = this.getReadableDatabase();
		// Select All Query
		String selectQuery = "select * from " + TABLE_CATEGORIES;

		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {

				// results.add(cursor.getInt(0));
				results.add(cursor.getString(2));

			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();

		return results;
	}

	public List<String> getAllCategoryNames() {
		List<String> results = new ArrayList<String>();
		SQLiteDatabase db = this.getReadableDatabase();
		// Select All Query
		String selectQuery = "select * from " + TABLE_CATEGORIES;

		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {

				// results.add(cursor.getInt(0));
				results.add(cursor.getString(2));

			} while (cursor.moveToNext());
		}
		cursor.close();
		db.close();

		return results;
	}

	
	
	// public List<Products> getAllproducts() {
	// List<Products> productList = new ArrayList<Products>();
	// // Select All Query
	// String selectQuery = "SELECT  * FROM " + TABLE_PRODUCTS;
	//
	// SQLiteDatabase db = this.getWritableDatabase();
	// Cursor cursor = db.rawQuery(selectQuery, null);
	//
	//
	// // looping through all rows and adding to list
	// if (cursor.moveToFirst()) {
	// do {
	// Products products = new Products();
	// products.setID(cursor.getString(2));
	// // Adding contact to list
	// productList.add(products);
	// } while (cursor.moveToNext());
	// }
	//
	// // return contact list
	// return productList;
	// }

}
