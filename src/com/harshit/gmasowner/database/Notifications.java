package com.harshit.gmasowner.database;

import android.util.Log;

public class Notifications {
	String _id;
	String email;
	String read;
	String type;
	String content;
	String timestamp;
	
	public Notifications()
	{
		//empty
	}
 public Notifications(String id, String email, String read, String type, String content, String timestamp) {
		this._id = id;
		this.email = email;
		this.read = read;
		this.type =type;
		this.content = content;
		this.timestamp = timestamp;
	}
 
	// getting ID
	public String getID() {
		return this._id;
	}

	// setting id
	public void setID(String id) {
		this._id = id;
	}
	

	public String getEmail() {
		return this.email;
	}

	// setting price
	public void setEmail(String email) {
		this.email = email;
	}
	
	

	public String getContent() {
		return this.content;
	}

	// setting price
	public void setContent(String Content) {
		this.content = Content;
	}
	
	

	public String getRead() {
		return this.read;
	}

	// setting price
	public void setRead(String Read) {
		this.read = Read;
	}
	
	

	public String getType() {
		return this.type;
	}

	// setting price
	public void settype(String type) {
		this.type = type;
	}
	
	
	

	public String gettime() {
		return this.timestamp;
	}

	// setting price
	public void setTime(String time) {
		this.timestamp = time;
	}
}
