package com.harshit.gmasowner.database;

import android.util.Log;

public class Products {

	// private variables
	String _id;
	String _title;
	String _stock;
	String _price;
	String _initial_stock;
	String _short_description;
	String _desc_image;
	String  issynced;
	String _catid;

	// Empty constructor
	public Products() {
		// TODO Auto-generated constructor stub {

	}

	// constructor
	public Products(String id, String name, String price, String image, String sync) {
		this._id = id;
		this._title = name;
		this._price = price;
		this._desc_image =image;
		this.issynced = sync;
	}

	public Products(String id, String name, String stock, String price,
			String initial_stock,String short_description,  String desc_image, String cat_id, String issync) {
		this._id = id;
		this._title = name;
		this._stock = stock;
		this._price = price;
		Log.d("image", desc_image);
		this._initial_stock = initial_stock;
		this._short_description = short_description;
		this._desc_image = desc_image;
		this._catid = cat_id;
		this.issynced = issync;
		

	}

	// constructor
	public Products(String id, String stock) {
		this._id = id;
		this._stock = stock;
	}

	// getting ID
	public String getID() {
		return this._id;
	}

	// setting id
	public void setID(String id) {
		this._id = id;
	}
	public String getCATID() {
		return this._catid;
	}

	// setting id
	public void setCATID(String cid) {
		this._catid = cid;
	}
	// getting ID
	public String getissynced() {
		return this.issynced;
	}

	// setting id
	public void setissynced(String _issynced) {
		this.issynced = _issynced;
	}

	public String getPrice() {
		return this._price;
	}

	// setting price
	public void setPrice(String Price) {
		this._price = Price;
	}

	public String getDescImage() {
		return this._desc_image;
	}

	// setting id
	public void setDescImage(String DescImage) {
		this._desc_image = DescImage;
	}
	

	public String getShortDescription() {
		return this._short_description;
	}

	// setting short description
	public void setShortDescription(String short_description) {
		this._short_description = short_description;
	}



	// setting name
	public void setName(String name) {
		this._title = name;
	}

	// getting phone number
	public String getStock() {
		return this._stock;
	}

	// setting phone number
	public void setStock(String stock) {
		this._stock = stock;
	}

	public String getInitialStock() {
		return this._initial_stock;
	}

	// setting initial stock
	public void setInitialStock(String intial_stock) {
		this._initial_stock = intial_stock;
	}


	public String getName() {
		// TODO Auto-generated method stub
		return this._title;
	}
}
