package com.harshit.gmasowner.database;

import android.util.Log;

public class Categories {
	
	//private variables
	String _category_id;
	String _category_name;
	String _parent_id;
	String _parent_name;
	String _issynced;
	public Categories()
	{
		//Empty Constructor
	}
	
	public Categories(String categoryid, String categoryname, String parentid, String issync)
	{
		this._category_id = categoryid;
		this._category_name = categoryname;
		this._parent_id =parentid;
		this._issynced = issync;
	}
	
	public String getID() {
		return this._category_id;
	}

	// setting id
	public void setId(String id) {
		this._category_id = id;
	}
	
	// getting ID
	public String getissynced() {
		return this._issynced;
	}

	// setting id
	public void setissynced(String issynced) {
		this._issynced = issynced;
	}
	
	public String getname() {
		return this._category_name;
	}

	// setting category name
	public void setname(String Name) {
		this._category_name =Name;
	}
	
	public String getparentid() {
		return this._parent_id;
	}

	// setting parent id
	public void setparentid(String id) {
		this._parent_id =id;
	}
	public String getparentname() {
		return this._parent_name;
	}

	// setting parent name
	public void setparentname(String name) {
		this._parent_name =name;
	}
	
	
}
