package com.harshit.gmasowner;

import java.util.ArrayList;
import java.util.List;

import com.harshit.gmasowner.database.Categories;
import com.harshit.gmasowner.database.DatabaseHandler;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class UpdateCategory extends Activity {
	EditText name, stock, price, short_description, initial_stock;
	Spinner category;
	private Toast toast = null;
	String string_name, string_category, label, string_parent_category,
			string_catid;
	Button update;
	int position = 0;
	List<Categories> myProductList = new ArrayList<Categories>();
	List<String> lables;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		final DatabaseHandler dbh = new DatabaseHandler(this);
		setContentView(R.layout.update_category);
		Bundle b = this.getIntent().getExtras();
		string_name = b.getString("webid");
		string_category = b.getString("parent_id");
		string_catid = b.getString("catid");
		name = (EditText) findViewById(R.id.updatecategory);
		category = (Spinner) findViewById(R.id.parentcategory);
		loadSpinnerData();
		Log.d("string_category", string_category);
		if(string_category=="null")
		{	Log.d("string name", string_name);
			category.setSelection(lables.size()-1);
		}else{
		category.setSelection(position);
		}
		update = (Button) findViewById(R.id.sendproduct);
		update.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				label = category.getSelectedItem().toString();
				// TODO Auto-generated method stub
				string_category = name.getText().toString();
				Log.d("label selected", label);
				if (label.equals("null")) {
					string_parent_category = "null";
				} else {

					string_parent_category = dbh.getIdFromCatName(label);
				}
				Log.d("parent id", string_parent_category);
				dbh.updateCategory(new Categories(string_catid,
						string_category, string_parent_category, "false"));
				showToast("Successfully added");
				Intent intent = new Intent(UpdateCategory.this,
						MainFragment.class);
				startActivity(intent);
			}
		});

		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(name.getWindowToken(), 0);

		name.setText(string_name);

	}

	private void loadSpinnerData() {
		// database handler
		DatabaseHandler dbh = new DatabaseHandler(MainFragment.currentContext);

		// Spinner Drop down elements
		lables = dbh.getAllCategoryIDs();
		lables.add("null");
		for (int i = 0; i < lables.size(); i++) {
			if (lables.get(i).equals(string_name)) {
				lables.remove(i);
			}

			if (lables.get(i).equals(string_category)) {
				position = i;

			}
			
		}
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(
				MainFragment.currentContext,
				android.R.layout.simple_spinner_item, lables);

		// Drop down layout style - list view with radio button
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		// attaching data adapter to spinner
		category.setAdapter(dataAdapter);
	}

	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		// On selecting a spinner item
		// / label = parent.getItemAtPosition(position).toString();

		// Showing selected spinner item
		// Toast.makeText(parent.getContext(), "You selected: " + label,
		// Toast.LENGTH_LONG).show();

	}

	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}

	private void showToast(String message) {
		if (this.toast == null) {
			// Create toast if found null, it would he the case of first call
			// only
			this.toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);

		} else if (this.toast.getView() == null) {
			// Toast not showing, so create new one
			this.toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);

		} else {
			// Updating toast message is showing
			this.toast.setText(message);
		}

		// Showing toast finally
		this.toast.show();
	}

}
