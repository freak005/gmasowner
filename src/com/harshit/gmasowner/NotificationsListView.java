package com.harshit.gmasowner;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class NotificationsListView extends ArrayAdapter<String>{
	private final Activity context;
	private final String[] type;
	private final String[] email;
	private final String[] content;
	private final String[] timestamp;
	
	public NotificationsListView(Activity context,String[] _type, String[] _content,String[] _email, String[] _time) {
		super(context, R.layout.myproduct_item_view, _type);
		this.context = context;
		this.type = _type;
		this.email = _email;
		this.content = _content;
		this.timestamp = _time;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater inflater = context.getLayoutInflater();
		View rowView= inflater.inflate(R.layout.notifications_list_view ,null, true);
		TextView texttype = (TextView) rowView.findViewById(R.id.type);
		TextView textemail = (TextView) rowView.findViewById(R.id.email); 
		TextView textcontent = (TextView) rowView.findViewById(R.id.content);
		TextView texttime = (TextView) rowView.findViewById(R.id.timestamp); 
		texttype.setText(type[position]);
		textemail.setText(email[position]);
		textcontent.setText(content[position]);
		texttime.setText(timestamp[position]);
		
		return rowView;
	}
	
}
