package com.harshit.gmasowner;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.harshit.gmasowner.database.Categories;
import com.harshit.gmasowner.database.DatabaseHandler;
import com.harshit.gmasowner.database.Notifications;
import com.harshit.gmasowner.database.Products;

public class MainFragment extends FragmentActivity {
	SharedPreferences sp;
	private ProgressDialog dialog;
	private ActionBar actionBar;
	private static String username;
	private boolean backPressedToExitOnce = false;
	private Toast toast = null;
	private static String encry_pass;
	SharedPreferences.Editor editor;
	DatabaseHandler db;
	static MyPageAdapter pageAdapter;
	static ViewPager pager;
	//public static String uri_login = "http://www.getmeashop.org/mobile/login/";
	public static String uri_login = "http://www.getmeashop.net/mobile/login/";
	static Context currentContext;
	private static Cookie getCSRFCookie;
	public boolean hassessionid =false;
	private String uri_get_product_details, uri_get_category_details, uri_logout,uri_get_notification_details;
	public PersistentCookieStore cookieStore;

	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		setContentView(R.layout.main_fragment);
		currentContext = this;
		SharedPreferences sp=getSharedPreferences("Users",Context.MODE_PRIVATE);
		username=sp.getString("userName", "");
		cookieStore = MainActivity.cookieStore;
		sp = MainActivity.sp;
		editor = sp.edit();
		CookieSyncManager.createInstance(currentContext);
		dialog = new ProgressDialog(currentContext);
		dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		// dialog.
		dialog.setMessage("Please wait...");
		dialog.setCancelable(false);
		db = new DatabaseHandler(currentContext);
		if(db.errorsync() || db.errorsynccategory())
		{
			Log.d("db.errorsync", String.valueOf(db.errorsync()));
			Log.d("db.errorsynccategory", String.valueOf(db.errorsynccategory()));
			showSync();
		}
		//if (!MainActivity.hasSession) {
			//encry_pass = this.getIntent().getStringExtra("password");
			//username = this.getIntent().getStringExtra("username");
		//}
		//else{
		//	username = sp.getString("username", "asdf");
		//}
		
		uri_get_product_details = "http://www.getmeashop.net/"+ username + "/api/mobile/product/?format=json";
		uri_get_category_details = "http://www.getmeashop.net/"+ username + "/api/mobile/category/?format=json";
		uri_get_notification_details = "http://www.getmeashop.net/"+ username + "/api/mobile/notification/?format=json";

		uri_logout = "http://www.getmeashop.net/" + username + "/logout/";
		hassessionid=MainActivity.hasSession;
		Log.d("has session id on create", String.valueOf(hassessionid));
		if(hassessionid)
		{
			getCSRFCookie=MainActivity.getCSRFCookie;
		//	Log.d("csrf cookie", getCSRFCookie.toString());
			showView();
		}
		else{
			new GetNewCSRF().execute("http://www.getmeashop.net/mobile/" + username+"/product/");
	}
	}
	// ===============intial get request=============================

		private class GetNewCSRF extends AsyncTask<String, Void, String> {
			@Override
			protected String doInBackground(String... arg0) {
				try {
					HttpParams httpParameters = new BasicHttpParams();
					HttpConnectionParams
							.setConnectionTimeout(httpParameters, 15000);
					HttpConnectionParams.setSoTimeout(httpParameters, 15000);
					HttpClient httpclient = new DefaultHttpClient(httpParameters);

					HttpGet getRequest = new HttpGet(URI.create(uri_login));

					HttpResponse getResponse = httpclient.execute(getRequest);

					System.out.println("GET CSRF status code:"
							+ getResponse.getStatusLine().getStatusCode());
					List<Cookie> getCookies = ((AbstractHttpClient) httpclient)
							.getCookieStore().getCookies();
					getCSRFCookie = getCookies.get(0);

				} catch (Exception e) {
					Log.d("GET CSRF Exception", e.toString());
				}
				return null;
			}

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				
				if (MainActivity.hasSession) {
					new GetProductAsyncTask().execute(uri_get_product_details);
		//			showView();
				} else {
				//	new LoginAsyncTask().execute(uri_login);
				}
			}

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				dialog.show();
			}

		}

		// ===============================LOGIN POST
		// REQUEST==========================================
//		private String postLoginDetails() {
//			try {
//				HttpParams httpParameters = new BasicHttpParams();
//
//				// Setup timeouts
//				HttpConnectionParams.setConnectionTimeout(httpParameters, 15000);
//				HttpConnectionParams.setSoTimeout(httpParameters, 15000);
//
//				HttpClient httpclient = new DefaultHttpClient(httpParameters);
//				((AbstractHttpClient) httpclient).getCookieStore().addCookie(
//						getCSRFCookie);
//
//				HttpPost httpPost = new HttpPost(uri_login);
//				//httpPost.addHeader("Host", "www.getmeashop.org");
//				httpPost.addHeader("Host", "www.getmeashop.net");
//				httpPost.addHeader("User-Agent", "Mozilla/5.0");
//				httpPost.addHeader("Accept",
//						"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
//				httpPost.addHeader("Accept-Language", "en-US,en;q=0.5");
//				httpPost.addHeader("Accept-Encoding", "gzip, deflate");
//				//httpPost.addHeader("Referer", "http://www.getmeashop.org/home/");
//				httpPost.addHeader("Referer", "http://www.getmeashop.net/home/");
//				httpPost.addHeader("Cookie",
//						"csrftoken=" + getCSRFCookie.getValue());
//				// ma=217405696.1532962016.1403170396.1403170396.1403170396.1;
//				// __utmb=217405696.1.10.1403170396; __utmc=217405696;
//				// __utmz=217405696.1403170396.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)");
//				httpPost.addHeader("Connection", "keep-alive");
//				httpPost.addHeader("Content-Type",
//						"application/x-www-form-urlencoded");
//				// httpPost.addHeader("Content-Length", "93");
//
//				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
//				nameValuePairs.add(new BasicNameValuePair("csrfmiddlewaretoken",
//						getCSRFCookie.getValue()));
//				nameValuePairs.add(new BasicNameValuePair("username", username));
//				nameValuePairs.add(new BasicNameValuePair("password", encry_pass));
//
//				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
//
//				HttpResponse httpResponse = httpclient.execute(httpPost);
//
//				List<Cookie> cookies = ((AbstractHttpClient) httpclient)
//						.getCookieStore().getCookies();
//				for (Cookie c : cookies) {
//					cookieStore.addCookie(c);
//					if (c.getName().equals("sessionid")) {
//						hassessionid = true;
//						editor.putBoolean("hassessionid", hassessionid);
//						
//						editor.putString("username", username);
//						editor.commit();
//					}
//				}
//
//				InputStream inputStream = httpResponse.getEntity().getContent();
//				String responseString = Utils
//						.convertInputStreamToString(inputStream);
//
//				System.out.println("login response:" + responseString);
//			} catch (UnsupportedEncodingException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (ClientProtocolException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//
//			return null;
//		}
//
//		private class LoginAsyncTask extends AsyncTask<String, Void, String> {
//			@Override
//			protected String doInBackground(String... urls) {
//				return uri_get_product_details;
//			//	return postLoginDetails();
//			}
//
//			// onPostExecute displays the results of the AsyncTask.
//			@Override
//			protected void onPostExecute(String result) {
//				Log.d("Post execute Login task" , String.valueOf(hassessionid));
//				if(hassessionid)
//				{
//				System.out.println("execution finished");
//				dialog.dismiss();
//				showToast("login successful");
//				new GetProductAsyncTask().execute(uri_get_product_details);
//				}
//				else{
//					//showToast("Login Failed");
//					Intent i = new Intent (MainFragment.this, MainActivity.class);
//					startActivity(i);
//				}
//			}
//		}

		//======================get products list ===============================================
				
				private class GetCategoryAsyncTask extends AsyncTask<String, Void, String> {

					@Override
					protected String doInBackground(String... arg0) {
						// TODO Auto-generated method stub
						try {
							// List<Cookie> savedCookies=cookieStore.getCookies();
							HttpClient httpclient = new DefaultHttpClient();
							// if (savedCookies != null) {
							// for (Cookie cookie:savedCookies) {
							// ((AbstractHttpClient)
							// httpclient).getCookieStore().addCookie(cookie);
							// }
							// }
							HttpGet request = new HttpGet(
									URI.create(uri_get_category_details));
							HttpResponse response = httpclient.execute(request);
							InputStream inputStream = response.getEntity().getContent();
							String responseString = Utils
									.convertInputStreamToString(inputStream);
							System.out.println(responseString);
							JSONObject jsonResponse = new JSONObject(responseString);
							JSONArray objects = jsonResponse.getJSONArray("objects");

							DatabaseHandler dbh = new DatabaseHandler(getApplicationContext());
							boolean databasecreated = true;
							if (doesDatabaseExist(currentContext, "productsmanager")) {
								dbh.removeCategory();
							}
							for (int i = 0; i < objects.length(); i++) {
								JSONObject object = objects.getJSONObject(i);
								dbh.addcategory(new Categories(object.getString("id"),
										object.getString("name"),object.getString("parent_category"),"true"));
							}

							// System.out.println("THE JSON FROM RESPONSE:" + jsonResponse);
						} catch (ClientProtocolException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						return null;
					}

					@Override
					protected void onPreExecute() {
						// TODO Auto-generated method stub
						dialog.show();
						super.onPreExecute();
					}

					@Override
					protected void onPostExecute(String result) {
						showView();
						dialog.dismiss();
						System.out.println("Finishing my product details async task");
					}
				}
				
		
		
		//======================get products list ===============================================
				
				private static boolean doesDatabaseExist(Context context, String dbName) {
					File dbFile = context.getDatabasePath(dbName);
					return dbFile.exists();
				}
				
				private class GetProductAsyncTask extends AsyncTask<String, Void, String> {

					@Override
					protected String doInBackground(String... arg0) {
						// TODO Auto-generated method stub
						try {
							// List<Cookie> savedCookies=cookieStore.getCookies();
							HttpClient httpclient = new DefaultHttpClient();
							// if (savedCookies != null) {
							// for (Cookie cookie:savedCookies) {
							// ((AbstractHttpClient)
							// httpclient).getCookieStore().addCookie(cookie);
							// }
							// }
							HttpGet request = new HttpGet(
									URI.create(uri_get_product_details));
							HttpResponse response = httpclient.execute(request);
							InputStream inputStream = response.getEntity().getContent();
							String responseString = Utils
									.convertInputStreamToString(inputStream);
							System.out.println(responseString);
							JSONObject jsonResponse = new JSONObject(responseString);
							JSONArray objects = jsonResponse.getJSONArray("objects");

							DatabaseHandler dbh = new DatabaseHandler(getApplicationContext());
							boolean databasecreated = true;
							if (doesDatabaseExist(currentContext, "productsmanager")) {
								dbh.removeAll();
							}
							for (int i = 0; i < objects.length(); i++) {
								JSONObject object = objects.getJSONObject(i);
								JSONArray jarray = object.getJSONArray("categories");
								String category =null;
								if(jarray.length()!=0)
								{
									category =jarray.get(0).toString();
								}else
								{
									category = "null";
								}
								Log.d("category id", category);
							
								String prodid = object.getString("id");
								dbh.addallproduct(new Products(object.getString("id"),
										object.getString("title"), object
												.getString("stock"), object
												.getString("price"), object
												.getString("initial_stock"),  object
												.getString("short_description"), "http://www.getmeashop.net"+
												object.getString("image"), category,"true"));
							}

							// System.out.println("THE JSON FROM RESPONSE:" + jsonResponse);
						} catch (ClientProtocolException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						return null;
					}

					@Override
					protected void onPreExecute() {
						// TODO Auto-generated method stub
						dialog.show();
						super.onPreExecute();
					}

					@Override
					protected void onPostExecute(String result) {
						dialog.dismiss();
						new GetCategoryAsyncTask().execute();
						new GetNotificationAsyncTask().execute();
						showView();
						System.out.println("Finishing my product details async task");
					}
				}
				
				
				
				
				//======================get products list ===============================================
						
						private class GetNotificationAsyncTask extends AsyncTask<String, Void, String> {

							@Override
							protected String doInBackground(String... arg0) {
								// TODO Auto-generated method stub
								try {
									// List<Cookie> savedCookies=cookieStore.getCookies();
									HttpClient httpclient = new DefaultHttpClient();
									// if (savedCookies != null) {
									// for (Cookie cookie:savedCookies) {
									// ((AbstractHttpClient)
									// httpclient).getCookieStore().addCookie(cookie);
									// }
									// }
									HttpGet request = new HttpGet(
											URI.create(uri_get_notification_details));
									HttpResponse response = httpclient.execute(request);
									InputStream inputStream = response.getEntity().getContent();
									String responseString = Utils
											.convertInputStreamToString(inputStream);
									System.out.println(responseString);
									JSONObject jsonResponse = new JSONObject(responseString);
									JSONArray objects = jsonResponse.getJSONArray("objects");

									DatabaseHandler dbh = new DatabaseHandler(getApplicationContext());
									boolean databasecreated = true;
//									if (doesDatabaseExist(currentContext, "productsmanager")) {
//										dbh.removeAll();
//									}
									for (int i = 0; i < objects.length(); i++) {
										JSONObject object = objects.getJSONObject(i);
										dbh.addallNotifications(new Notifications(object.getString("id"),
												object.getString("email"), object
														.getString("read"), object
														.getString("type"), object
														.getString("content"),  object
														.getString("timestamp")));
									}

									// System.out.println("THE JSON FROM RESPONSE:" + jsonResponse);
								} catch (ClientProtocolException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								return null;
							}

							@Override
							protected void onPreExecute() {
								// TODO Auto-generated method stub
								dialog.show();
								super.onPreExecute();
							}

							@Override
							protected void onPostExecute(String result) {
								dialog.dismiss();
								new GetCategoryAsyncTask().execute();
						//		showView();
								System.out.println("Finishing my product details async task");
							}
						}
										
				
//=======================AddFragment===============================================================================
		private void showView() {
			if(dialog.isShowing())
				dialog.dismiss();
			for (Cookie cookie : cookieStore.getCookies()) {
				String cookieString = cookie.getName() + "=" + cookie.getValue()
						+ "; domain=" + cookie.getDomain();
				CookieManager.getInstance().setCookie(cookie.getDomain(),
						cookieString);
			}
			CookieSyncManager.getInstance().sync();

			// swipe tabs view related code
			pageAdapter = new MyPageAdapter(getSupportFragmentManager());
//			TabPageIndicator titleIndicator = (TabPageIndicator) findViewById(R.id.titles);
//			titleIndicator.setViewPager(pager);
			pageAdapter.addTitlePage("Home");
			pageAdapter.addProduct("Add Product");
			pageAdapter.addCategory("Add Category");
			pageAdapter.manageproduct("Manage Products");
			pageAdapter.manageCategory("Manage Category");
			pageAdapter.addNotificationNo("Notifications");
			pager = (ViewPager) findViewById(R.id.pager);
			pager.setOffscreenPageLimit(10);
			
			pager.setPageTransformer(true, new ZoomOutPageTransformer());
			pager.setAdapter(pageAdapter);

//			webview.loadUrl(uri_dashboard);
			// ((Activity)currentContext).setContentView(webview);
		}



	// ===========================MyPageAdapterClass====================================================================
	class MyPageAdapter extends FragmentPagerAdapter {

		private List<Fragment> fragments;
		private List<String> titles;

		public MyPageAdapter(FragmentManager fm) {
			super(fm);
			this.fragments = new ArrayList<Fragment>();
			this.titles = new ArrayList<String>();
		}

		public void manageproduct(String string) {
			// TODO Auto-generated method stub
			Fragment manage_category = new CategoryFragment();
			this.fragments.add(manage_category);
			this.titles.add(string);
		}

		// public void addItem(String url, String title) {
		// WebFragment myFragment = new WebFragment();
		// Bundle args = new Bundle();
		// args.putString("url", url);
		// myFragment.setArguments(args);
		// myFragment
		// .setDialog(currentContext.getApplicationContext(), dialog);
		// this.fragments.add(myFragment);
		// this.titles.add(title);
		//
		// }
		
		public void addTitlePage(String home)
		{
			Fragment titlepage = new TitlePage();

			 Bundle args = new Bundle(); 
			this.fragments.add(titlepage);
			this.titles.add(home);
		}
		
		public void addProduct(String title)
		{
			Fragment addproduct = new AddProducts();
			this.fragments.add(addproduct);
			this.titles.add(title);
			
		}
		
		public void manageCategory(String title)
		{
			Fragment managecategory = new ManageCategory();
			this.fragments.add(managecategory);
			this.titles.add(title);
			
		}
		
		public void addCategory(String title)
		{
			Fragment addcategory = new AddCategory();
			this.fragments.add(addcategory);
			this.titles.add(title);
			
		}
		
		
		public void addNotificationNo(String title)
		{
			Fragment addnotification = new NotificationsList();
			this.fragments.add(addnotification);
			this.titles.add(title);
			
		}
		// public void addCategoryFragment(String title) {
		// CategoryFragment myFragment = new CategoryFragment();
		// Bundle args = new Bundle();
		// myFragment.init(currentContext);
		// this.titles.add(title);
		// this.fragments.add((Fragment) myFragment);
		// }

		@Override
		public Fragment getItem(int pos) {
			// TODO Auto-generated method stub
			return this.fragments.get(pos);
		}

		public CharSequence getPageTitle(int pos) {
			return this.titles.get(pos);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return this.fragments.size();
		}
	}

	// ==========SendALLCategoryStock==============================================================================
	private class SendAllCategoryAsyncTask extends
			AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... urls) {
			return postAllCategorySendDetails();
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			dialog.show();
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			dialog.dismiss();
			showToast("successfully send");
			Intent intent = new Intent(MainFragment.currentContext,MainFragment.class);
			startActivity(intent);
		}
	}

	private String postAllCategorySendDetails() {
		// TODO Auto-generated method stub
		try {
			HttpParams httpParameters = new BasicHttpParams();

			// Setup timeouts
			HttpConnectionParams.setConnectionTimeout(httpParameters, 15000);
			HttpConnectionParams.setSoTimeout(httpParameters, 15000);
			Log.d("cookie cookiestore", cookieStore.getCookies().get(1).toString());
			HttpClient httpclients = new DefaultHttpClient(httpParameters);
//			Log.d("csrf cookie", getCSRFCookie.toString());
//			((AbstractHttpClient) httpclient).getCookieStore().addCookie(
//					getCSRFCookie);
			((AbstractHttpClient) httpclients).getCookieStore().addCookie(cookieStore.getCookies().get(1));
			HttpPost httpPost = new HttpPost(
					"http://www.getmeashop.net/mobile/" + username+"/category/");
			ArrayList<Categories> prod = (ArrayList<Categories>) db.getNotSyncedCategory();
//			for (Categories cn : prod) {
//
//				JSONObject jobject = new JSONObject();
//			}
			
			JSONObject jsonObject = new JSONObject();
			if (prod != null && prod.size() > 0) {
				JSONArray jsonarray = new JSONArray();
				for (Categories cn : prod) {

					JSONObject jobject = new JSONObject();
					jobject.put("id", cn.getID());
					jobject.put("name", cn.getname());
					jobject.put("parent_category", cn.getparentid());
					jsonarray.put(jobject);
				}
				jsonObject.put("categories", jsonarray);
				Log.d("json", jsonObject.toString());
			}
			 List<NameValuePair> nameValuePairs = new
			 ArrayList<NameValuePair>();
//			 nameValuePairs.add(new BasicNameValuePair("stock",
//			 stock));
			 nameValuePairs.add(new BasicNameValuePair("username",
			 username));;
			 nameValuePairs.add(new BasicNameValuePair("prod_json", jsonObject.toString()));
			
			 httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			HttpResponse httpResponse = httpclients.execute(httpPost);


			InputStream inputStream = httpResponse.getEntity().getContent();
			String responseString = Utils
					.convertInputStreamToString(inputStream);

			System.out.println("send response:" + responseString);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// // TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "success";
	}

	

	// ==========SendALLProductsStock==============================================================================
	private class SendAllProductsAsyncTask extends
			AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... urls) {
			return postAllProductsSendDetails();
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			dialog.show();
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			dialog.dismiss();
			showToast("successfully send");
		}
	}

	private String postAllProductsSendDetails() {
		// TODO Auto-generated method stub
		try {
			List<Cookie> loginCookies=new ArrayList<Cookie>();
			loginCookies=cookieStore.getCookies();
			BasicCookieStore cookieStore=new BasicCookieStore();
			Log.d("login cookies 0", String.valueOf(loginCookies.size()));
			Log.d("login cookies 1", loginCookies.get(1).toString());
			cookieStore.addCookie(loginCookies.get(0));
			cookieStore.addCookie(loginCookies.get(1));
			
			HttpContext localContext=new BasicHttpContext();
			localContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
			
			HttpParams httpParameters = new BasicHttpParams();

			// Setup timeouts
			HttpConnectionParams.setConnectionTimeout(httpParameters, 15000);
			HttpConnectionParams.setSoTimeout(httpParameters, 15000);

			HttpClient httpclient = new DefaultHttpClient(httpParameters);
			((AbstractHttpClient) httpclient).getCookieStore().addCookie(
					getCSRFCookie);
//			System.out.println("cookie_mainfragment: "+loginCookies.get(0)+" "+loginCookies.get(1));

			HttpPost httpPost = new HttpPost(
					"http://www.getmeashop.net/mobile/" + username+"/product/");
			ArrayList<Products> prod = (ArrayList<Products>) db.getNotSyncedProducts();
			
			
			JSONObject jsonObject = new JSONObject();
			if (prod != null && prod.size() > 0) {
				JSONArray jsonarray = new JSONArray();
				for (Products cn : prod) {

					JSONObject jobject = new JSONObject();
					jobject.put("id", cn.getID());
					jobject.put("title", cn.getName());
					jobject.put("price", cn.getPrice());
					jobject.put("stock", cn.getStock());
					jobject.put("short_description", cn.getShortDescription());
					jobject.put("initial_stock", cn.getInitialStock());
	//				jobject.put("parent_category", cn.getCATID());
					jobject.put("image", cn.getDescImage());
					jsonarray.put(jobject);
				}
				jsonObject.put("products", jsonarray);
				Log.d("json", jsonObject.toString());
			}
			 List<NameValuePair> nameValuePairs = new
			 ArrayList<NameValuePair>();
//			 nameValuePairs.add(new BasicNameValuePair("stock",
//			 stock));
			 nameValuePairs.add(new BasicNameValuePair("username",
			 username));;
			 nameValuePairs.add(new BasicNameValuePair("prod_json", jsonObject.toString()));
			
			 httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			HttpResponse httpResponse = httpclient.execute(httpPost,localContext);

			// List<Cookie> cookies = ((AbstractHttpClient) httpclient)
			// .getCookieStore().getCookies();
			// for (Cookie c : cookies) {
			// cookieStore.addCookie(c);
			// if (c.getName().equals("sessionid")) {
			// SharedPreferences.Editor editor = sp.edit();
			// editor.putString("username", username);
			// editor.commit();
			// }
			// }

			InputStream inputStream = httpResponse.getEntity().getContent();
			String responseString = Utils
					.convertInputStreamToString(inputStream);
			System.out.println(responseString);
			JSONObject jobject = new JSONObject(responseString);
			JSONArray objects = jobject.getJSONArray("products");

			DatabaseHandler dbh = new DatabaseHandler(getApplicationContext());
			for (int i = 0; i < objects.length(); i++) {
				JSONObject object = objects.getJSONObject(i);
				dbh.updateSingleProduct(new Products(object.getString("id"),
						object.getString("title"),object.getString("price"),object.getString("image"),"true"));
			}


			System.out.println("send response:" + responseString);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// // TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "success";
	}

	
	// ==========================================================================================

	// ===================================================Logout async
	// task================================================
	private class LogoutAsyncTask extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... arg0) {
			// TODO Auto-generated method stub
			try {
				List<Cookie> savedCookies = cookieStore.getCookies();
				System.out.println("cookies:" +savedCookies.get(0)+" "+savedCookies.get(1));
				HttpClient httpclient = new DefaultHttpClient();
				if (savedCookies != null) {
					for (Cookie cookie : savedCookies) {
						((AbstractHttpClient) httpclient).getCookieStore()
								.addCookie(cookie);
					}
				}
				HttpGet request = new HttpGet(URI.create(uri_logout));
				HttpResponse response = httpclient.execute(request);
				System.out.println("The LOGOUT status code: "
						+ response.getStatusLine().getStatusCode());

			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			MainActivity.hasSession = false;
			DatabaseHandler dbh = new DatabaseHandler(currentContext);
			dbh.removeAll();
			 dbh.deletedatabase();
			 finish();
		        Intent intent = new Intent(Intent.ACTION_MAIN); 
		        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
		        intent.addCategory(Intent.CATEGORY_HOME); 
		        startActivity(intent);
			editor.clear();
		//	cookieStore.deleteCookie(getCSRFCookie);
			editor.commit();
			cookieStore.clear();
			dialog.dismiss();
			finish();
			
//			// showWebView();
//			 Intent i = new Intent(getApplicationContext(),MainActivity.class);
//			 startActivity(i);
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			dialog.show();
		}
		
		
	}

	@Override
	public void onBackPressed() {
	
		if (backPressedToExitOnce) {
			alertDialogBeforeFinish();
	//		this.finish();
	//		this.moveTaskToBack(true);
		//	this.finish();
	//		super.onBackPressed();
		} else {
			this.backPressedToExitOnce = true;
			showToast("Press again to exit");
			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					backPressedToExitOnce = false;
				}
			}, 2000);
		}
		
	}
	private void alertDialogBeforeFinish() {
		// TODO Auto-generated method stub
		
		if(db.errorsync() || db.errorsynccategory())
		{
			Log.d("db.errorsync", String.valueOf(db.errorsync()));
			Log.d("db.errorsynccategory", String.valueOf(db.errorsynccategory()));
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				currentContext);
 
			// set title
			alertDialogBuilder.setTitle("Your Title");
 
			// set dialog message
			alertDialogBuilder
				.setMessage("Do you want to sync data to server")
				.setCancelable(false)
				.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, close
						// current activity
						new SendAllProductsAsyncTask().execute();
						new SendAllCategoryAsyncTask().execute();
					}
				  })
				.setNegativeButton("No",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						MainFragment.this.finish();
						MainFragment.this.moveTaskToBack(true);
					}
				});
 
				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();
 
				// show it
				alertDialog.show();
			}
	}
	private void showToast(String message) {
		if (this.toast == null) {
			// Create toast if found null, it would he the case of first call
			// only
			this.toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);

		} else if (this.toast.getView() == null) {
			// Toast not showing, so create new one
			this.toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);

		} else {
			// Updating toast message is showing
			this.toast.setText(message);
		}

		// Showing toast finally
		this.toast.show();
	}
	

	//@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public boolean onCreateOptionsMenu(Menu menu) {

		getMenuInflater().inflate(R.menu.main, menu);
		menu.add("Sync to Server");
		menu.add("Sync from Server");
		menu.add("Logout");
		

		
		return true;
	}

	@SuppressLint("NewApi")
	public void showSync()
	{
		 ActionBar actionBar = getActionBar();
		    actionBar.setDisplayOptions(actionBar.getDisplayOptions()
		            | ActionBar.DISPLAY_SHOW_CUSTOM);
		    ImageView imageView = new ImageView(actionBar.getThemedContext());
		    imageView.setScaleType(ImageView.ScaleType.CENTER);
		    imageView.setImageResource(android.R.drawable.ic_popup_disk_full);
		    ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(
		            ActionBar.LayoutParams.WRAP_CONTENT,
		            ActionBar.LayoutParams.WRAP_CONTENT, Gravity.RIGHT
		                    | Gravity.CENTER_VERTICAL);
		    layoutParams.rightMargin = 40;
		    imageView.setLayoutParams(layoutParams);
		    actionBar.setCustomView(imageView);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		} else if (item.getTitle().equals("Logout")) {
			new LogoutAsyncTask().execute();
		}else if (item.getTitle().equals("Sync to Server")) {
			new SendAllProductsAsyncTask().execute();
			new SendAllCategoryAsyncTask().execute();
		}
		else if (item.getTitle().equals("Sync from Server")) {
			new GetProductAsyncTask().execute();
			new GetCategoryAsyncTask().execute();
		}
		return super.onOptionsItemSelected(item);
	}

	private void killToast() {
		if (this.toast != null) {
			this.toast.cancel();
		}
	}

}
