package com.harshit.gmasowner;

import java.io.ByteArrayOutputStream;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.harshit.gmasowner.database.DatabaseHandler;
import com.harshit.gmasowner.database.Products;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;



public class UpdateProduct extends Activity {
	EditText name,stock, price, short_description, initial_stock;
	Spinner category;

	private static final int CAMERA_REQUEST = 1888; 
	ImageView imgview;
	String encodedImage, string_id;

	private Toast toast = null;
	Button sendProduct;
	String  string_name, string_stock, string_price, string_short_description, string_initial_stock, string_imgview;
	String s_name,s_stock,s_price,s_sd,s_is,s_img;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.update_product);
		Bundle b=this.getIntent().getExtras();
		final DatabaseHandler dbhandle = new DatabaseHandler(this);
		string_id = b.getString("id");
		string_name=b.getString("webid");
		string_stock = b.getString("stock");
		string_price = b.getString("price");
		string_short_description = b.getString("sd");
		string_initial_stock = b.getString("initial_stock");
		string_imgview = b.getString("imageid");
		name= (EditText)findViewById(R.id.addproduct);
		name.clearFocus();
		
		InputMethodManager imm = (InputMethodManager)getSystemService(
			      Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(name.getWindowToken(), 0);
		stock = (EditText)findViewById(R.id.stock);
		price = (EditText)findViewById(R.id.price);
		short_description = (EditText)findViewById(R.id.short_decription);
		initial_stock = (EditText)findViewById(R.id.initial_stock);
		sendProduct = (Button)findViewById(R.id.sendproduct);
		category = (Spinner)findViewById(R.id.category);
		loadSpinnerData();
		imgview = (ImageView)findViewById(R.id.imageView1);
		encodedImage = "www.getmeashop.org/null";
		imgview.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE); 
                startActivityForResult(cameraIntent, CAMERA_REQUEST); 
            }
        });
		sendProduct.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				s_name = name.getText().toString();
				s_stock = stock.getText().toString();
				s_price = price.getText().toString();
				s_is = initial_stock.getText().toString();
				s_sd = short_description.getText().toString();
				String label=null;
				if(category.getSelectedItem()!=null)
					label = category.getSelectedItem().toString();
				dbhandle.updateProduct(new Products(string_id,s_name,s_stock,s_price,s_is,s_sd,encodedImage,label,"false"));
				showToast("Successfully added");
				Intent intent = new Intent (UpdateProduct.this,MainFragment.class);
				startActivity(intent);
			}
		});
		name.setText(string_name);
		short_description.setText(string_short_description);
		stock.setText(string_stock);
		price.setText(string_price);
		initial_stock.setText(string_initial_stock);
		UrlImageViewHelper.setUrlDrawable(imgview,string_imgview);
		if(string_imgview.equals("http://www.getmeashop.org/null"))
		{
			imgview.setImageResource(R.drawable.product);
		}

	}
	
	private void loadSpinnerData() {
        // database handler
        DatabaseHandler dbh = new DatabaseHandler(MainFragment.currentContext);
 
        // Spinner Drop down elements
        List<String> lables = dbh.getAllCategoryIDs();
 
        // Creating adapter for spinner
//        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
//                android.R.layout.simple_spinner_item, lables);
        ArrayAdapter<String>dataAdapter = new ArrayAdapter<String>(MainFragment.currentContext, android.R.layout.simple_spinner_item,lables);
 
        // Drop down layout style - list view with radio button
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
 
        // attaching data adapter to spinner
        category.setAdapter(dataAdapter);
    }
 
    public void onItemSelected(AdapterView<?> parent, View view, int position,
            long id) {
        // On selecting a spinner item
        String label = parent.getItemAtPosition(position).toString();
 
        // Showing selected spinner item
        Toast.makeText(parent.getContext(), "You selected: " + label,
                Toast.LENGTH_LONG).show();
 
    }
 
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
 
    }
	private void showToast(String message) {
		if (this.toast == null) {
			// Create toast if found null, it would he the case of first call
			// only
			this.toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);

		} else if (this.toast.getView() == null) {
			// Toast not showing, so create new one
			this.toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);

		} else {
			// Updating toast message is showing
			this.toast.setText(message);
		}

		// Showing toast finally
		this.toast.show();
	}
	 public void onActivityResult(int requestCode, int resultCode, Intent data) {  
	        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {  
	            Bitmap photo = (Bitmap) data.getExtras().get("data"); 
	            imgview.setImageBitmap(photo);
	            ByteArrayOutputStream baos = new ByteArrayOutputStream();
	            photo.compress(Bitmap.CompressFormat.JPEG, 100, baos);
	            byte[] imageBytes = baos.toByteArray();
	            encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
	            
	        } 
	
	
	 }

}
