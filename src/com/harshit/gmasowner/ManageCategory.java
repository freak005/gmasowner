package com.harshit.gmasowner;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.harshit.gmasowner.database.Categories;
import com.harshit.gmasowner.database.DatabaseHandler;
import com.harshit.gmasowner.database.Products;

public class ManageCategory extends Fragment {
	MySimpleSearchAdapter mAdapter;
	TextView textJson;
	private String[ ] catname, parentname,catid;
	List<Categories> myProductList = new ArrayList<Categories>();
	ListView list;
	Button btnSearch, btnLeft;
	EditText mtxt;
	DatabaseHandler dbh ;
	List<String> items = new ArrayList<String>();
	List<String> itemsParent = new ArrayList<String>();
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view= inflater.inflate(R.layout.frag_view_category, container,false);
		dbh = new DatabaseHandler(MainFragment.currentContext);
		mAdapter = new MySimpleSearchAdapter((Activity)MainFragment.currentContext);
		btnSearch = (Button)view.findViewById(R.id.btnSearch);
		btnLeft = (Button)view. findViewById(R.id.btnLeft);
		mtxt = (EditText)view. findViewById(R.id.edSearch);
		list=(ListView)view.findViewById(R.id.listView_myProducts);
		myProductList = dbh.getAllCategory();
		showView();
		return view;
	}
	

	private void showView(){
		catname=new String[myProductList.size()];
		parentname=new String[myProductList.size()];
		catid = new String[myProductList.size()];

		
 		int i = 0;
		for (Categories cn :myProductList) {
			String log = "Id: " + cn.getID() + " ,Name: " + cn.getname()
					+ " ,Phone: " + cn.getparentid() + ",Price  " + cn.getparentname();
			catname[i] = cn.getname();
			String n = "null";
			if(cn.getparentid().equals(n))
			{
				Log.d("show view catid", cn.getparentid());
				parentname[i] = "null";
			}
			else{

				Log.d("show view catname", cn.getparentid());
				parentname[i] =dbh.getCatNameFromId(cn.getparentid());
			}
			catid[i] = cn.getID();
			
			i++;
			Log.d("Name: ", log);
		}
		mtxt.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				if (0 != mtxt.getText().length()) {
					String spnId = mtxt.getText().toString();
//					setSearchResult(spnId);
				} else {
//					setData();
				}
			}
		});
	//	btnLeft.setOnClickListener(this);
		//btnSearch.setOnClickListener(this);
//		setData();

		list.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View v,
					final int position, long id) {
				
				Intent inte = new Intent(MainFragment.currentContext, UpdateCategory.class);
				Bundle b=new Bundle();
				b.putString("webid",catname[position]);
				b.putString("parent_id", parentname[position]);
				b.putString("catid", catid[position]);
				inte.putExtras(b);
				startActivity(inte);
			}
		});

		CategoryList adapter = new
			CategoryList((Activity) MainFragment.currentContext,catname,parentname);
		list.setAdapter(adapter);    
	}
	/*
	public void setData() {
		items = new ArrayList<String>();
		itemsParent = new ArrayList<String>();

		mAdapter = new MySimpleSearchAdapter((Activity)MainFragment.currentContext);
		for (int i = 0; i < catname.length; i++) {
			mAdapter.addItems(catname[i], parentname[i]);
			itemsStock.add(price[i]);
			itemsURL.add(imageId[i]);
			items.add(catname[i]);
		}
//		list.setOnItemClickListener(this);
		list.setAdapter(mAdapter);
	}
	
	public void setSearchResult(String str) {
		mAdapter = new MySimpleSearchAdapter((Activity)MainFragment.currentContext);
		int i = 0;
		for (String temp : items) {

			if (temp.toLowerCase().contains(str.toLowerCase())) {
				System.out.println("items: " + temp);
				mAdapter.addItems(temp, price[i], imageId[i]);
				Log.d("title", web[i]);
				Log.d("stock", price[i]);

			}
			i++;
		}
		// for(int i =0; i<items.size();i++)
		// {
		// if(web[0].toLowerCase().contains(str.toLowerCase()))
		// {
		// mAdapter.addItems(web[i],price[i],imageId[i]);
		// break;
		// }
		// }
		// }
		list.setAdapter(mAdapter);
	}
*/
}
